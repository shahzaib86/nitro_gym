<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('b_permissions')->truncate();
        DB::table('b_permissions')->insert([
            ['id'=>1,'name'=>"right-management",'slug'=>"right-management"],
            ['id'=>2,'name'=>"content-management",'slug'=>"content-management"],
            ['id'=>3,'name'=>"user-management",'slug'=>"user-management"],
            ['id'=>4,'name'=>"settings",'slug'=>"settings"],
            ['id'=>5,'name'=>"categories",'slug'=>"categories"],
            ['id'=>6,'name'=>"admins",'slug'=>"admins"],
            ['id'=>7,'name'=>"members",'slug'=>"members"],
        ]);
        DB::table('b_roles_permissions')->truncate();
        DB::table('b_roles_permissions')->insert([
            ['role_id'=>1,'permission_id'=>1],
            ['role_id'=>1,'permission_id'=>2],
            ['role_id'=>1,'permission_id'=>3],
            ['role_id'=>1,'permission_id'=>4],
            ['role_id'=>1,'permission_id'=>5],
            ['role_id'=>1,'permission_id'=>6],
            ['role_id'=>1,'permission_id'=>7]
        ]);
    }
}
