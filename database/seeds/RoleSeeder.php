<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('b_roles')->truncate();
        DB::table('b_roles')->insert([
            'id' => 1,
            'name' => 'SuperAdmin',
            'slug' => 'superadmin',
        ]);
        
        DB::table('b_roles')->insert([
            'id' => 2,
            'name' => 'Admin',
            'slug' => 'admin',
        ]);
        
    }
}
