<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('b_email_template')->truncate();
        DB::table('b_email_template')->insert([
            'template' => 'welcome_email',
            'type' => 'HTML Template here',
        ]);
    }
}
