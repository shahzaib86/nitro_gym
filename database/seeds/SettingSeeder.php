<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('b_settings')->truncate();

        DB::table('b_settings')->insert([
            'key' => 'app_version',
            'value' => '1.0',
            'title' => 'Application Version',
            'description' => ''
        ]);
        
        // DB::table('settings')->insert([
        //     'key' => 'show_blog',
        //     'value' => '0',
        //     'title' => 'Show Blogs',
        //     'description' => '',
        //     'json_params' => '{ "type":"dropdown","data":[{"label":"Yes","value":1},{"label":"No","value":0}] }'
        // ]);

    }
}
