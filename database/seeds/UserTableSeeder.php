<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->truncate();

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'role_id' => '1',
            // Admin@123
            'password' => '$2y$10$Fgw2mcZUwMDuYgkDuwxuM.z6Vr7ZxhdhE0QQk1tXFtbs9yqQG75bC',
            'avatar' => 'profile_pictures/avatar.jpg'
        ]);
        DB::table('b_users_roles')->truncate();
        DB::table('b_users_roles')->insert([
            'user_id'=>1,'role_id'=>1
        ]);
    }
}
