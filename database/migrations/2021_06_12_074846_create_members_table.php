<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();

            $table->string('member_no',30);
            $table->string('name',50);
            $table->string('email',50)->nullable();
            $table->string('mobile',50)->nullable();
            $table->string('phone',50)->nullable();

            $table->string('father_name',50);
            $table->string('gender',10)->default('male');

            $table->string('age',10)->nullable();
            $table->string('height',10)->nullable();
            $table->string('weight',10)->nullable();

            $table->string('occupation',20)->nullable();
            $table->string('timing')->nullable();

            $table->string('address',500)->nullable();
            $table->string('cnic',20)->nullable();

            $table->string('branch')->nullable();
            $table->text('medical_problem')->nullable();

            $table->string('package')->default('0');
            $table->date('join_date')->nullable();

            $table->string('package_id')->nullable();

            $table->boolean('status')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
