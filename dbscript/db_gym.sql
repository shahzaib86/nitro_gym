/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.4.17-MariaDB : Database - db_gym
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_gym` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `db_gym`;

/*Table structure for table `b_activity_log` */

DROP TABLE IF EXISTS `b_activity_log`;

CREATE TABLE `b_activity_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `activity_template_id` int(10) unsigned NOT NULL,
  `activity_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `json_params` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `b_activity_log_activity_template_id_index` (`activity_template_id`),
  KEY `b_activity_log_user_id_index` (`user_id`),
  CONSTRAINT `b_activity_log_activity_template_id_foreign` FOREIGN KEY (`activity_template_id`) REFERENCES `b_activity_template` (`id`),
  CONSTRAINT `b_activity_log_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `b_activity_log` */

/*Table structure for table `b_activity_template` */

DROP TABLE IF EXISTS `b_activity_template`;

CREATE TABLE `b_activity_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `b_activity_template` */

/*Table structure for table `b_page` */

DROP TABLE IF EXISTS `b_page`;

CREATE TABLE `b_page` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `b_page` */

/*Table structure for table `b_permissions` */

DROP TABLE IF EXISTS `b_permissions`;

CREATE TABLE `b_permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `b_permissions` */

insert  into `b_permissions`(`id`,`name`,`slug`) values 
(1,'right-management','right-management'),
(2,'settings','settings'),
(3,'categories','categories'),
(4,'admins','admins'),
(5,'members','members'),
(6,'packages','packages');

/*Table structure for table `b_roles` */

DROP TABLE IF EXISTS `b_roles`;

CREATE TABLE `b_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `b_roles` */

insert  into `b_roles`(`id`,`name`,`slug`) values 
(1,'SuperAdmin','superadmin'),
(2,'Admin','admin');

/*Table structure for table `b_roles_permissions` */

DROP TABLE IF EXISTS `b_roles_permissions`;

CREATE TABLE `b_roles_permissions` (
  `role_id` bigint(20) unsigned NOT NULL,
  `permission_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `b_roles_permissions` */

insert  into `b_roles_permissions`(`role_id`,`permission_id`) values 
(1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(1,6);

/*Table structure for table `b_settings` */

DROP TABLE IF EXISTS `b_settings`;

CREATE TABLE `b_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `json_params` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `b_settings` */

insert  into `b_settings`(`id`,`key`,`value`,`title`,`description`,`json_params`) values 
(2,'address','Plot No A-123 No 4 Nazimabad, Karachi','Gym Address',NULL,NULL),
(3,'phone','0300 1234567','Phone',NULL,NULL),
(4,'email','info@nitrogen.com','Email',NULL,NULL);

/*Table structure for table `b_users_roles` */

DROP TABLE IF EXISTS `b_users_roles`;

CREATE TABLE `b_users_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `b_users_roles` */

insert  into `b_users_roles`(`user_id`,`role_id`) values 
(1,1);

/*Table structure for table `fees` */

DROP TABLE IF EXISTS `fees`;

CREATE TABLE `fees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `amount` double NOT NULL DEFAULT 0,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `package_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fees_member_id_index` (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `fees` */

insert  into `fees`(`id`,`member_id`,`amount`,`date`,`created_at`,`updated_at`,`package_id`) values 
(2,2,7000,'2021-06-16','2021-06-15 18:24:46','2021-06-15 18:24:46',NULL),
(3,1,2000,'2021-07-13','2021-06-16 16:34:26','2021-06-16 16:34:26','1,'),
(4,6,7000,'2021-06-16','2021-06-16 17:14:30','2021-06-16 17:14:30','1,2,4,'),
(6,6,7000,'2021-10-22','2021-06-17 19:35:32','2021-06-17 19:35:32','1,2,4,');

/*Table structure for table `members` */

DROP TABLE IF EXISTS `members`;

CREATE TABLE `members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `member_no` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male',
  `age` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cnic` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medical_problem` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `join_date` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `package_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `members` */

insert  into `members`(`id`,`member_no`,`name`,`email`,`mobile`,`phone`,`father_name`,`gender`,`age`,`height`,`weight`,`occupation`,`timing`,`address`,`cnic`,`branch`,`medical_problem`,`package`,`join_date`,`status`,`created_at`,`updated_at`,`package_id`) values 
(1,'N2021-1','Testing','testing@gmail.com','03006265478',NULL,'Father Name','male','20','6 Feet','80 Kg',NULL,'6:00 pm','This is test','4210147848574',NULL,'test','5000','2021-06-12',1,'2021-06-12 09:56:49','2021-06-12 10:06:44',NULL),
(2,'N2021-2','New member','asdasd@gmail.com','03006262959',NULL,'This is','male','12','12','12',NULL,'123','asdadas ad','4141474747845',NULL,'asdas dasd','5000','2021-06-13',1,'2021-06-13 17:36:05','2021-06-13 17:36:05',NULL),
(3,'N2021-3','test deals','wow@gmail.com','03006295987','03412415642','this is it','male','18','20','20','student','10 pm','adas dasd aa','4741541251451','1231','ad asda sd a','7000','2021-06-17',1,'2021-06-15 17:43:57','2021-06-15 18:03:39','1,2,4,'),
(6,'N2021-4','New Member','asdadasd@gmail.com','03002154879',NULL,'Test name','male','20',NULL,NULL,'student','10 pm','this is test','4141412154512',NULL,NULL,'7000','2021-06-16',1,'2021-06-16 17:14:30','2021-06-16 17:14:30','1,2,4,');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2020_10_08_082403_create_b_page_table',1),
(4,'2020_10_08_082434_create_b_activity_template_table',1),
(5,'2020_10_08_082441_create_b_activity_log_table',1),
(6,'2020_10_08_122755_create_b_settings_table',1),
(7,'2020_10_08_131400_create_b_permissions_table',1),
(8,'2020_10_08_131440_create_b_roles_table',1),
(9,'2020_10_08_131501_create_b_roles_permissions_table',1),
(10,'2020_10_12_080524_create_b_users_roles_table',1),
(11,'2021_06_12_074846_create_members_table',1),
(13,'2021_06_12_074944_create_fees_table',2),
(14,'2021_06_15_164524_create_package_table',3);

/*Table structure for table `package` */

DROP TABLE IF EXISTS `package`;

CREATE TABLE `package` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL DEFAULT 0,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `package` */

insert  into `package`(`id`,`name`,`amount`,`type`,`status`,`created_at`,`updated_at`) values 
(1,'Registration',2000,'Mix Timing',1,'2021-06-15 17:04:35','2021-06-15 17:04:35'),
(2,'Cardio',1000,'Mix Timing',1,'2021-06-15 17:04:53','2021-06-15 17:04:53'),
(3,'Testing',5000,'Female Timing',1,'2021-06-15 17:27:39','2021-06-15 17:27:39'),
(4,'Trainer',4000,'Mix Timing',1,'2021-06-15 17:28:02','2021-06-15 17:28:02'),
(5,'Female Trainer',1000,'Female Timing',1,'2021-06-15 17:28:11','2021-06-15 17:28:11');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fcm_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`phone`,`google_id`,`facebook_id`,`avatar`,`fcm_token`,`role_id`,`code`,`status`,`is_deleted`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Admin','admin@admin.com',NULL,'$2y$10$Fgw2mcZUwMDuYgkDuwxuM.z6Vr7ZxhdhE0QQk1tXFtbs9yqQG75bC',NULL,NULL,NULL,'profile_pictures/avatar.jpg',NULL,1,NULL,0,0,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
