<?php

Route::prefix('admin')->group(function () {
    //------------------- ADMIN AUTH ---------------------//
    Route::get('/login','Boiler\AdminController@index')->name('admin.login');
    Route::post('/login','Boiler\AdminController@login')->name('admin.login.post');
});

Route::prefix('admin')->middleware(['auth'])->group(function () {
    
    Route::get('/home','Boiler\AdminController@dashboard')->name('admin.home');
    
    //--------- Admin User -----------------------------------//
    Route::get('/admins','Boiler\AdminUserController@index')->name('admin.show');
    Route::post('/admins/fetch','Boiler\AdminUserController@fetch')->name('admin.fetch');
    Route::get('/admins/create','Boiler\AdminUserController@create')->name('admin.create');
    Route::post('/admins/store','Boiler\AdminUserController@store')->name('admin.store');
    Route::get('/admins/edit/{id}','Boiler\AdminUserController@edit')->name('admin.edit');
    Route::post('/admins/update/{id}','Boiler\AdminUserController@update')->name('admin.update');
    Route::get('/admins/delete/{id}','Boiler\AdminUserController@delete')->name('admin.delete');


    //--------- Right Management -----------------------------//
    Route::get('/roles-and-permission/roles','Boiler\RightController@show_roles')->name('roles.show');
    Route::get('/roles-and-permission/roles/add','Boiler\RightController@add_role')->name('role.add');
    Route::post('/roles-and-permission/roles/add','Boiler\RightController@create_role')->name('role.create');
    Route::get('/roles-and-permission/roles/edit/{role}','Boiler\RightController@edit_role')->name('roles.edit');
    Route::post('/roles-and-permission/roles/update/{role}','Boiler\RightController@update_role')->name('role.update');


    //-------Email tempelate ----------------------//
    Route::get('/email-template/view','Boiler\EmailTemplateController@view')->name('email.template.show');
    Route::post('/email-template/delete','Boiler\EmailTemplateController@delete')->name('email.template.delete');
    Route::get('/email-template/edit/{id}','Boiler\EmailTemplateController@edit')->name('email.template.edit');
    Route::post('email-template/edit/{id}','Boiler\EmailTemplateController@edit')->name('email.template.edit');

    //-------Pages------------//
    Route::get('/pages/view','Boiler\PageController@view')->name('pages.show');
    Route::get('pages/add','Boiler\PageController@add')->name('pages.add');
    Route::post('pages/add','Boiler\PageController@add')->name('pages.add');
    Route::get('pages/edit/{id}','Boiler\PageController@edit')->name('pages.edit');
    Route::post('pages/edit/{id}','Boiler\PageController@edit')->name('pages.edit');
    Route::get('pages/delete/{id}','Boiler\PageController@delete')->name('pages.delete');

    //-------SETTINGS--------------------
    Route::get('/settings/view','Boiler\SettingController@index')->name('admin.settings');
    Route::post('/settings/update','Boiler\SettingController@update')->name('admin.settings.update');

    //-------Blogs Category------------//
    Route::get('/blogs-category/view','Boiler\BlogCategoryController@view')->name('blogs_category.show');
    Route::get('blogs-category/add','Boiler\BlogCategoryController@add')->name('blogs_category.add');
    Route::post('blogs-category/add','Boiler\BlogCategoryController@add')->name('blogs_category.add');
    Route::get('blogs-category/edit/{id}','Boiler\BlogCategoryController@edit')->name('blogs_category.edit');
    Route::post('blogs-category/edit/{id}','Boiler\BlogCategoryController@edit')->name('blogs_category.edit');
    Route::get('blogs-category/delete/{id}','Boiler\BlogCategoryController@delete')->name('blogs_category.delete');

    //-------Blogs------------//
    Route::get('/blogs','Boiler\BlogController@view')->name('blogs.show');
    Route::post('/blogs/fetch','Boiler\BlogController@fetch')->name('blogs.fetch');
    Route::get('blogs/add','Boiler\BlogController@add')->name('blogs.add');
    Route::post('blogs/add','Boiler\BlogController@add')->name('blogs.add');
    Route::get('blogs/edit/{id}','Boiler\BlogController@edit')->name('blogs.edit');
    Route::post('blogs/edit/{id}','Boiler\BlogController@edit')->name('blogs.edit');
    Route::get('blogs/delete/{id}','Boiler\BlogController@delete')->name('blogs.delete');

    //-------Activiity Logs------------//
    Route::get('/activitylogs','Boiler\ActivityLogsController@view')->name('activitylogs.show');
    Route::post('/activitylogs/fetch','Boiler\ActivityLogsController@fetch')->name('activitylogs.fetch');

    //------------HOME BANNER
    Route::get('home/banners','Boiler\HomeController@banners')->name('home.banners');
    Route::post('home/banners/update/sequence','Boiler\HomeController@updateSequenceBanners')->name('home.banner.update.sequence');
    Route::get('home/banner/add','Boiler\HomeController@createBanner')->name('home.banner.add');
    Route::post('home/banner/add','Boiler\HomeController@saveBanner')->name('home.banner.save');
    Route::get('home/banner/edit/{id}','Boiler\HomeController@editBanner')->name('home.banner.edit');
    Route::post('home/banner/update/{id}','Boiler\HomeController@updateBanner')->name('home.banner.update');

    //--------- Members -----------------------------------//
    Route::get('/members','Boiler\MemberController@index')->name('members');
    Route::post('/member/fetch','Boiler\MemberController@fetch')->name('member.fetch');
    Route::get('/member/create','Boiler\MemberController@create')->name('member.create');
    Route::post('/member/store','Boiler\MemberController@store')->name('member.store');
    Route::get('/member/edit/{id}','Boiler\MemberController@edit')->name('member.edit');
    Route::post('/member/update/{id}','Boiler\MemberController@update')->name('member.update');
    Route::get('/member/delete/{id}','Boiler\MemberController@delete')->name('member.delete');


    Route::get('/fees/report','Boiler\FeesController@index')->name('fees.report');
    Route::get('/financial/report','Boiler\FeesController@revenue')->name('fees.revenue');
    Route::get('/fees/add','Boiler\FeesController@addFees')->name('fees.add');
    Route::post('/fees/store','Boiler\FeesController@store')->name('fees.store');

    Route::get('/report/due-date','Boiler\FeesController@dueDateReport')->name('fees.report.due.date');



    //--------- Package -----------------------------------//
    Route::get('/packages','Boiler\PackageController@index')->name('packages');
    Route::post('/package/fetch','Boiler\PackageController@fetch')->name('package.fetch');
    Route::get('/package/create','Boiler\PackageController@create')->name('package.create');
    Route::post('/package/store','Boiler\PackageController@store')->name('package.store');
    Route::get('/package/edit/{id}','Boiler\PackageController@edit')->name('package.edit');
    Route::post('/package/update/{id}','Boiler\PackageController@update')->name('package.update');
    Route::get('/package/delete/{id}','Boiler\PackageController@delete')->name('package.delete');

    Route::get('/expenses','Boiler\ExpenseController@index')->name('expenses');
    Route::post('/expense/fetch','Boiler\ExpenseController@fetch')->name('expense.fetch');
    Route::get('/expense/add','Boiler\ExpenseController@create')->name('expense.add');
    Route::post('/expense/store','Boiler\ExpenseController@store')->name('expense.store');
    Route::get('/expense/edit/{id}','Boiler\ExpenseController@edit')->name('expense.edit');
    Route::post('/expense/update/{id}','Boiler\ExpenseController@update')->name('expense.update');
    Route::get('/expense/delete/{id}','Boiler\ExpenseController@delete')->name('expense.delete');


});
