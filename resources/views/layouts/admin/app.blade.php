<!DOCTYPE html>
{{-- <html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> --}}
<html lang="pt_BR /opt/google/chrome/chrome">

<head>
    <meta charset="utf-8" />
    <title> {{ (isset($title))?$title:' ' }} {{ config('app.name', 'Laravel') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured laravel starter project which can be used to build custom webb application."
        name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('admin/images/favicon.png') }}">

    <!-- Notification css (Toastr) -->
    <link href="{{ asset('admin/libs/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- App css -->
    {{-- <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{ asset('admin/css/bootstrap-dark.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ asset('admin/css/app.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{ asset('admin/css/app-dark.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/main.css') }}" rel="stylesheet" type="text/css" />

    {{-- ADDITIONAL CDNS --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote-bs4.min.css"
        crossorigin="anonymous" />
    <link href="{{ asset('admin/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css"> --}}

    <link href="{{ asset('admin/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/libs/datatables/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/libs/datatables/select.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @stack('css')
</head>

<body data-topbar="dark">

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        <div class="navbar-custom">
            <ul class="list-unstyled topnav-menu float-right mb-0">

                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#"
                        role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{asset(Auth::user()->avatar)}}" alt="" class="rounded-circle">
                        <span class="pro-user-name ml-1">
                            {{Auth::user()->name}} <i class="mdi mdi-chevron-down"></i>
                        </span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">

                        {{-- <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i class="fe-user"></i>
                            <span>My Account</span>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i class="fe-lock"></i>
                            <span>Lock Screen</span>
                        </a>

                        <div class="dropdown-divider"></div> --}}

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fe-log-out"></i>
                            <span>{{ __('Logout') }}</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>

                    </div>

                </li>

            </ul>

            <!-- LOGO -->
            <div class="logo-box">
                <a href="{{ route('admin.home') }}" class="logo text-center">
                    <span class="logo-lg">
                        <img src="{{asset('admin/images/nitrogen-dark.png')}}" alt="" height="24">
                        <!-- <span class="logo-lg-text-light">Xeria</span> -->
                    </span>
                    <span class="logo-sm">
                        <!-- <span class="logo-sm-text-dark">X</span> -->
                        <img src="{{asset('admin/images/nitrogen-dark.png')}}" alt="" height="24">
                    </span>
                </a>
            </div>

            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <li>
                    <button class="button-menu-mobile disable-btn waves-effect">
                        <i class="fe-menu"></i>
                    </button>
                </li>

                <li>
                    <h4 class="page-title-main text-capitalize"> @yield('page_header') </h4>
                </li>

            </ul>
        </div>
        <!-- end Topbar -->

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu" style="background:#2c333e;">

            <div class="slimscroll-menu">

                <!-- User box -->
                {{-- <div class="user-box text-center">
                    <img src="{{ asset(Auth::user()->avatar) }}" alt="" class="rounded-circle img-thumbnail avatar-lg">
                    <div class="dropdown">
                        <a href="#" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block text-capitalize"
                            data-toggle="dropdown">{{Auth::user()->name}}</a>
                    </div>
                    <p class="text-muted">Admin Head</p>

                </div> --}}

                <!--- Sidemenu -->
                <div id="sidebar-menu">

                    <ul class="metismenu" id="side-menu">

                        <li class="menu-title">Navigation</li>

                        <li>
                            <a href="{{ route('admin.home') }}">
                                <i class="mdi mdi-view-dashboard"></i>
                                <span class=""> Dashboard </span>
                            </a>
                        </li>

                        

                        @can('members')
                        <li>
                            <a href="{{ route('members') }}">
                                <i class="mdi mdi-account-group"></i>
                                <span class="">Members</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('member.create') }}">
                                <i class="mdi mdi-account"></i>
                                <span class="">Add Member</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('fees.add') }}">
                                <i class="mdi mdi-receipt"></i>
                                <span class="">Add Fees</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('fees.report') }}">
                                <i class="mdi mdi-google-spreadsheet"></i>
                                <span class="">Fees Report</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('fees.report.due.date') }}">
                                <i class="mdi mdi-google-spreadsheet"></i>
                                <span class="">Due Date Report</span>
                            </a>
                        </li>
                        
                        @endcan

                        @can('financial-reports')
                        <li>
                            <a href="{{ route('fees.revenue') }}">
                                <i class="mdi mdi-google-spreadsheet"></i>
                                <span class="">Financial Report</span>
                            </a>
                        </li>
                        @endcan

                        @can('expenses')
                        <li>
                            <a href="{{ route('expenses') }}">
                                <i class="mdi mdi-account-group"></i>
                                <span class="">Expenses</span>
                            </a>
                        </li>
                        @endcan

                        @can('packages')
                        <li>
                            <a href="{{ route('packages') }}">
                                <i class="mdi mdi-account-group"></i>
                                <span class="">Packages</span>
                            </a>
                        </li>
                        @endcan

                        @can('admins')
                        <li>
                            <a href="{{ route('admin.show') }}">
                                <i class="mdi mdi-account-group"></i>
                                <span class="">Admins</span>
                            </a>
                        </li>
                        @endcan
             
                        @can('settings')
                        <li>
                            <a href="{{ route('admin.settings') }}">
                                <i class="mdi mdi-settings"></i>
                                <span class="">Settings</span>
                            </a>
                        </li>
                        @endcan

                        @can('right-management')
                        <li>
                            <a href="{{ route('roles.show') }}">
                                <i class="fas fa-users-cog"></i>
                                <span class="">Rights Management</span>
                            </a>
                        </li>
                        @endcan


                    </ul>

                </div>
                <!-- End Sidebar -->

                <div class="clearfix"></div>

            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <img src="{{asset('admin/images/n-dark.png')}}" alt="" style="position: absolute;
            height: 80%;
            right: 0;
            bottom: 60px;
            opacity: 0.1;">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">

                    @yield('content')

                </div>
                <!-- container -->

            </div>
            <!-- content -->

            <!-- Footer Start -->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            {{ Carbon\Carbon::parse(now())->isoFormat('YYYY') }} &copy; <a href="#">
                                NITROGEN </a>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- Vendor js -->
    <script src="{{ asset('admin/js/vendor.min.js') }}"></script>

    <!-- knob plugin -->
    <script src="{{ asset('admin/libs/jquery-knob/jquery.knob.min.js') }}"></script>

    <!--Morris Chart-->
    {{-- <script src="{{ asset('admin/libs/morris-js/morris.min.js') }}"></script>
    --}}
    {{-- <script src="{{ asset('admin/libs/raphael/raphael.min.js') }}"></script>
    --}}

    <!-- Dashboard init js-->
    {{-- <script src="{{asset('admin/js/pages/dashboard.init.js')}}"></script> --}}

    <!-- Toastr js -->
    <script src="{{ asset('admin/libs/toastr/toastr.min.js') }}"></script>

    {{-- SWEETALERT --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- App js -->
    <script src="{{ asset('admin/js/app.min.js') }}"></script>

    <!-- Main js -->
    <script src="{{ asset('admin/js/main.js') }}"></script>

    {{-- ADDITIONAL CDNS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote-bs4.js" crossorigin="anonymous">
    </script>
    
    <script src="{{ asset('admin/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/libs/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('admin/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/libs/datatables/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('admin/libs/select2/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            // $('.dropify').dropify();
            // $('.summernote').summernote();
            // $('.datatable').DataTable();
            @if(Session::has('message'))
                toastr["{{Session::get('type')}}"]("{{Session::get('message')}}","{{Session::get('title')}}")
            @endif

        })

    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    @stack('scripts')
</body>

</html>