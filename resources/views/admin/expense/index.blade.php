@extends('layouts.admin.app')

@push('css')
<!-- third party css -->
<link href="{{asset('admin/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endpush
@section('page_header') Expenses @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <a href="{{ route('expense.add') }}" class="my-2 btn btn-success">
                Add New <i class="fa fa-plus"></i>
            </a>
            <div class="card-box table-responsive">

                <div class="row">

                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="users">Date From</label>
                      <input type="date" class="form-control" name="date_from" id="date_from" />
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="users">Date To</label>
                      <input type="date" class="form-control" name="date_to" id="date_to" />
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label style="color:#ffffff00">Action</label> <br>
                      <button class="btn btn-success submit_filters">Submit</button>
                      <a href="{{route('expenses')}}" class="btn btn-warning ml-2">Clear Filters</a>
                    </div>
                  </div>
                  
                </div>

                <table id="adminlistTable"  class="table table-bordered dt-responsive nowrap" style="width:100%;">
                    <thead>
                        <tr>
                            <th width="10px">#</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Note</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>

        </div>
    </div>


@endsection

@push('scripts')
<!-- third party js -->
<script src="{{asset('admin/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<!-- third party js ends -->
<script>

search_start();
$('#member_status').change(function(){
  search_start();
});

$('.submit_filters').click(function(){
  search_start();
});

function search_start(){
      if ($.fn.DataTable.isDataTable("#adminlistTable")) $("#adminlistTable").DataTable().destroy();
      //Start getting filters

      let date_from = $('#date_from').val();
      let date_to = $('#date_to').val();
      
      $('#adminlistTable').DataTable({
        "scrollX": true,
       processing: true,
       serverSide: true,
       ajax: {
            url: "{{route('expense.fetch')}}",
            type: "POST",
            data: {
              "_token": "{{ csrf_token() }}",
              date_from:date_from,
              date_to:date_to,
            }
        },
       createdRow: function( row, data, dataIndex ) {
            // $(row).attr('class', 'data-row click_detail');
            // $(row).attr('data-id', data.id);
            // console.log(row,data);
        },
       columns: [
                { data: 'id' },
                { data: 'date',
                  render: function(data, type, row) {
                    return row.m_date;
                  }
                },
                { data: 'amount',
                  render: function(data, type, row) {
                    return "Rs. "+row.amount;
                  }
                },
                { data: 'note' },
                { data: 'user.name' },
                { data: 'id',
                  render: function(data, type, row) {
                    let e_url = ("{{ route('expense.edit',['id'=>'-id-'])}}").replace('-id-', row.id);
                    let d_url = ("{{ route('expense.delete',['id'=>'-id-'])}}").replace('-id-', row.id);
                    return `<a class="btn btn-info btn-sm text-white cursor-pointer" href="${e_url}"> <i class="fa fa-edit"></i> </a>
                    <a class="btn btn-danger btn-sm text-white cursor-pointer" href="${d_url}"> <i class="fa fa-trash"></i>`;
                  }
                },
                               
              ],
              "drawCallback": function( settings ) {
                //   initShowDetailsMethod();
              } //end drawcallback
          });
      }

</script>
@endpush
