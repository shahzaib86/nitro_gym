@extends('layouts.admin.app',['title'=>'Expense - ' ])

@push('css')
<style>
    .select2-selection--single {
        height: 36px !important;
        border: 1px solid #ced4da !important;
    }
    .select2-selection__rendered {
        height: 36px !important;
        line-height: 36px !important;
        color: unset !important;
        padding: 0rem .8em !important;
    }

    .add_package,
    .remove_package {
        cursor: pointer;
    }

    .selected_box .widget-user {
        background: #caff00;
    }

    .selected_box .text-primary {
        color: #606060!important;
    }

    .selected_box h5{
        color: #5a5a5a !important;
    }
    .select2-results__options {
        color:#000;
    }
    .select2-container--default .select2-results__option[aria-selected=true] {
        background-color: #3a4250 !important;
        color: #e1e9ee !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #444 !important;
    }
</style>

@endpush
@section('page_header') @if($item) Update @else Add @endif Expense @endsection
@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card-box">

            <h4 class="mt-0 mb-3 header-title">
                @if($item) Update @else Add @endif Expense
            </h4>

            <form role="form" method="POST" @if($item) action="{{route('expense.update',['id'=>$item->id])}}" @else action="{{route('expense.store')}}" @endif  >
                @csrf
                
                <div class="form-group">
                    <label for="name">Date</label>
                    <input type="date" class="form-control" name="date" @if($item) value="{{Carbon\Carbon::parse($item->date)->isoFormat('YYYY-MM-DD')}}" @else value="" @endif required>
                    @error('date')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="name">Amount</label>
                    <input type="text" class="form-control" name="amount" id="amount" placeholder="Enter amount" @if($item) value="{{$item->amount}}" @else value="{{old('amount')?old('amount'):0}}" @endif required>
                    @error('amount')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="note">Note</label>
                    <textarea name="note" id="note" class="form-control">@if($item){{$item->note}}@else{{old('note')}}@endif</textarea>
                    @error('note')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>
                
                <input type="submit" class="btn btn-primary" value="@if($item) Update @else Submit @endif" >
            </form>
        </div>
    </div>

</div>

@endsection

@push('scripts')
<script>

$(document).ready(function(){


});

</script>

@endpush