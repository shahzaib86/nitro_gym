@extends('layouts.admin.app')
@section('page_header') Add  @endsection
@section('content')
    <div class="row">

        <div class="col-6">
            <div class="card-box ">
                <div class="row">
                    {{-- LEFT COLUMN --}}
                    <div class="col-12">
                        <form method="post" enctype="multipart/form-data">
                            @csrf
                            <!-- text input -->

                            <div class="form-group">
                                <label for="name">Name </label>
                                <input type="text" id="name" name="name" value="{{ old('name') }} " class="form-control"
                                    autofocus placeholder="Enter Name">
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="description">Description </label>
                                <textarea class="summernote" name="description" id="description"></textarea>
                                @error('description')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="food_type_id">Food Type </label>
                                <select name="food_type" class="form-control" id="food_type_id">
                                    <option class="form-control" value="">Select</option>
                                    @foreach ($food_type as $food_type)
                                        <option class="form-control" value="{{ $food_type->id }}">{{ $food_type->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('food_type')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="food_category_id ">Food Category </label>
                                <select name="food_category" class="form-control" id="food_category_id">
                                    <option class="form-control" value="">Select</option>
                                    @foreach ($food_categories as $food_category)
                                        <option class="form-control" value="{{ $food_category->id }}">
                                            {{ $food_category->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('food_category')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="mr-2">Has Nuts </label>
                                <label>
                                    <input type="radio" value="yes" checked name="has_nuts"> Yes
                                </label>
                                <label for=""></label>
                                <label>
                                    <input type="radio" value="no" name="has_nuts"> No
                                </label>
                                @error('has_nuts')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="mr-2">Has Lactose </label>
                                <label>
                                    <input type="radio" value="yes" name="has_lactose"> Yes
                                </label>
                                <label for=""></label>
                                <label>
                                    <input type="radio" value="no" checked name="has_lactose"> No
                                </label>
                                @error('has_lactose')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="calorie_count">Calorie Count </label>
                                <input type="number" id="calorie_count" value="{{ old('calorie_count') }}"
                                    name="calorie_count" class="form-control" autofocus placeholder="Enter Count">
                                @error('calorie_count')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-info pull-right">Add</button>
                        </form>
                    </div>
                    {{-- END LEFT COLUMN --}}
                    {{-- /////////////// --}}
                    {{-- RIGHT COLUMN --}}
                    <div class="col-6">
                    </div>
                    {{-- END RIGHT COLUMN --}}
                </div>
            </div>

        </div>
    @endsection
