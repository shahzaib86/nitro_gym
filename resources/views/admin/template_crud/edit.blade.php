@extends('layouts.admin.app')
@section('page_header') Edit  @endsection
@section('content')
    <div class="row">

        <div class="col-6">
            <div class="card-box ">
                <div class="row">
                    {{-- LEFT COLUMN --}}
                    <div class="col-12">
                        <form method="post" enctype="multipart/form-data">
                            @csrf
                            <!-- text input -->

                            <div class="form-group">
                                <label for="name">Name </label>
                                <input type="text" id="name" name="name" value=""
                                    class="form-control" autofocus placeholder="Enter Name">
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="description">Description </label>
                                <textarea class="summernote" name="description"
                                    id="description"></textarea>
                                @error('description')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="food_type_id">Food Type </label>
                                <select name="food_type" class="form-control" id="food_type_id">
                                    <option class="form-control" value="">Select</option>
                                    {{-- @foreach ($food_type as $food_type)
                                        <option class="form-control"
                                            {{ $food_type->id == $respective_data->food_type_id ? 'selected' : '' }}
                                            value="{{ $food_type->id }}">{{ $food_type->name }}
                                        </option>
                                    @endforeach --}}
                                </select>
                                @error('food_type')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="mr-2">Has Nuts </label>
                                <label>
                                    @php $respective_data->has_nuts = "yes" @endphp
                                    <input type="radio" {{ $respective_data->has_nuts == 'yes' ? 'checked' : '' }}
                                        value="yes" name="has_nuts"> Yes
                                </label>
                                <label for=""></label>
                                <label>
                                    <input type="radio" {{ $respective_data->has_nuts == 'no' ? 'checked' : '' }} value="no"
                                        name="has_nuts"> No
                                </label>
                                @error('has_nuts')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>


                            <button type="submit" class="btn btn-info pull-right">Update</button>
                        </form>
                    </div>
                    {{-- END LEFT COLUMN --}}
                    {{-- /////////////// --}}
                    {{-- RIGHT COLUMN --}}
                    <div class="col-6">
                    </div>
                    {{-- END RIGHT COLUMN --}}
                </div>
            </div>

        </div>
    @endsection
