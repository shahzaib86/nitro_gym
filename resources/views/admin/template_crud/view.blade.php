@extends('layouts.admin.app')

@push('css')
<!-- third party css -->
<link href="{{asset('admin/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endpush
@section('page_header') All @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <a href="{{ route('food_item-add') }}" class="my-2 btn btn-success">
                Add New
                <i class="fa fa-plus"></i>
            </a>
            <div class="card-box table-responsive">

                <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-form-label">Category</label>
                        <select id="category" class="form-control" >
                            <option value=""> Select to search </option>
                            {{-- @foreach(App\Models\FoodCategory::all() as $item)
                              <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-form-label">Type</label>
                        <select id="type" class="form-control" >
                            <option value=""> Select to search </option>
                            {{-- @foreach(App\Models\FoodType::all() as $item)
                              <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-form-label">Status</label>
                        <select id="food_status" class="form-control" >
                            {{-- @foreach(App\Models\FoodType::all() as $item) --}}
                              <option selected value="1">Enable</option>
                              <option value="0">Disable</option>
                            {{-- @endforeach --}}
                        </select>
                    </div>
                </div>
            </div>

                <table id="fooditemsTable"  class="table table-bordered dt-responsive nowrap" style="width:100%;"">
                    <thead>
                        <tr>
                            <th width="10px">#</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th width="10px">Nuts</th>
                            <th width="10px">Lactose</th>
                            <th width="10px">Status</th>
                            <th width="200px">Action</th>


                        </tr>
                    </thead>
                    <tbody>
                        {{-- @php $i = 0 @endphp
                        @foreach ($result as $result)
                            @php $i++ @endphp
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $result->name }} </td>
                                <td>
                                    <a href="{{ route('food_item-edit', [$result->id]) }}" class="btn btn-info ">
                                        Edit </a>
                                    <a href="{{ route('food_item-delete', [$result->id]) }}"
                                        class="delete_user btn btn-danger " data-user_id=""> Delete</a>
                                </td>
                            </tr>
                        @endforeach --}}
                    </tbody>
                </table>
            </div>

        </div>
    </div>




@endsection


{{-- @push('scripts')
<!-- third party js -->
<script src="{{asset('admin/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<!-- third party js ends -->
<script>


search_start();

$('#category').change(function(){
    search_start();
})

$('#type').change(function(){
    search_start();
})

$('#food_status').change(function(){
    search_start();
})

function search_start(){
      if ($.fn.DataTable.isDataTable("#fooditemsTable")) $("#fooditemsTable").DataTable().destroy();
      //Start getting filters

      let category = $('#category').val();
      let type = $('#type').val();
      let status = $('#food_status').val();
      
      $('#fooditemsTable').DataTable({
        "scrollX": true,
       processing: true,
       serverSide: true,
       ajax: {
            url: "{{route('food_item-fetch')}}",
            type: "POST",
            data: {
              "_token": "{{ csrf_token() }}",
              category:category,
              type:type,
              status:status,
            }
        },
       createdRow: function( row, data, dataIndex ) {
            // $(row).attr('class', 'data-row click_detail');
            // $(row).attr('data-id', data.id);
            // console.log(row,data);
        },
       columns: [
                { data: 'id' },
                { data: 'name' },
                { data: 'category.name' },
                { data: 'type.name' },
                { data: 'has_nuts' ,
                  render: function ( data, type, row ) {
                    if(row.has_nuts =='yes'){
                      return '<span class="badge badge-success">Yes</span>';
                    } else {
                      return '<span class="badge badge-danger">No</span>';
                    }
                  }
                },
                { data: 'has_lactose' ,
                  render: function ( data, type, row ) {
                    if(row.has_lactose == 'yes'){
                      return '<span class="badge badge-success">Yes</span>';
                    } else {
                      return '<span class="badge badge-danger">No</span>';
                    }
                  }
                },
                { data: 'status' ,
                  render: function ( data, type, row ) {
                    if(row.status == 1){
                      return '<span class="badge badge-success">Enable</span>';
                    } else {
                      return '<span class="badge badge-danger">Disabled</span>';
                    }
                  }
                },
                { data: 'id',
                  render: function(data, type, row) {
                    let e_url = ("{{ route('food_item-edit',['id'=>'-id-'])}}").replace('-id-', row.id);
                    let d_url = ("{{ route('food_item-delete',['id'=>'-id-'])}}").replace('-id-', row.id);
                    return `<a class="btn btn-info btn-sm text-white cursor-pointer" href="${e_url}"> <i class="fa fa-edit"></i> </a>
                    <a class="btn btn-danger btn-sm text-white cursor-pointer" href="${d_url}"> <i class="fa fa-trash"></i>`;
                  }
                },
                               
              ],
              "drawCallback": function( settings ) {
                //   $('#search-result-div').show();
                //   $('#search-overlay').hide();
                //   initShowDetailsMethod();
              } //end drawcallback
    });
}

</script>
@endpush --}}
