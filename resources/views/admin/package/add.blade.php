@extends('layouts.admin.app',['title'=>'Package - ' ])

@push('css')


@endpush
@section('page_header') Package @endsection
@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card-box">

            <h4 class="mt-0 mb-3 header-title">
                @if ($user) Update Package 
                @else Add New Package @endif
            </h4>

            <form role="form" method="POST" 
            @if ($user)
                action="{{route('package.update',['id'=>$user->id])}}"
            @else
                action="{{route('package.store')}}"
            @endif>
                @csrf

                <div class="form-group">
                    <label for="name">Title</label>
                    <input type="text" class="form-control" name="name" id="name" @if($user) value="{{$user->name}}" @else value="{{old('name')}}" @endif placeholder="Enter title">
                    @error('name')
                        <small id="nameHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="amount">Amount (Rs)</label>
                    <input type="text" class="form-control" name="amount" id="amount" @if($user) value="{{$user->amount}}" @else value="{{old('amount')}}" @endif placeholder="Enter amount">
                    @error('amount')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="type">Type</label>
                    <select class="form-control" id="type" name="type">

                        <option value=""> Select </option>
                        <option value="Mix Timing" @if( $user && $user->type == 'Mix Timing' ) selected @endif> Mix Timing </option>
                        <option value="Female Timing" @if( $user && $user->type == 'Female Timing' ) selected @endif> Female Timing </option>

                    </select>
                    @error('type')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="checkbox checkbox-primary pl-3">
                        <input type="checkbox" class="control-input" id="status" name="status" @if( $user && $user->status == 1 ) checked @elseif( !$user ) checked @endif >
                        <label for="status">
                            Enable
                        </label>
                    </div>
                </div>
                
                <input type="submit" class="btn btn-primary" value="Submit" >
            </form>
        </div>
    </div>
</div>


@endsection


@push('scripts')



@endpush