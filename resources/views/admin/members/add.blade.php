@extends('layouts.admin.app',['title'=>'Member - ' ])

@push('css')
<style>

    .add_package,
    .remove_package {
        cursor: pointer;
    }

    .selected_box .widget-user {
        background: #caff00;
    }

    .selected_box .text-primary {
        color: #606060!important;
    }

    .selected_box h5{
        color: #5a5a5a !important;
    }

/* input[type="date"]::-webkit-datetime-edit-text { color: transparent;  }

input[type="date"]::-webkit-datetime-edit, input[type="date"]::-webkit-inner-spin-button, input[type="date"]::-webkit-clear-button {
  color: #fff;
  position: relative;
}

input[type="date"]::-webkit-datetime-edit-year-field{
  position: absolute !important;
  border-left:1px solid #8c8c8c;
  padding: 2px;
  color:#fff;
  left: 56px;
}

input[type="date"]::-webkit-datetime-edit-month-field{
  position: absolute !important;
  border-left:1px solid #8c8c8c;
  padding: 2px;
  color:#fff;
  left: 26px;
}


input[type="date"]::-webkit-datetime-edit-day-field{
  position: absolute !important;
  color:#fff;
  padding: 2px;
  left: 4px;
  
} */

</style>

@endpush
@section('page_header') Member @endsection
@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card-box">

            <h4 class="mt-0 mb-3 header-title">
                @if ($user) Update Member 
                @else Add New Member @endif
            </h4>

            <form role="form" method="POST" 
            @if ($user)
                action="{{route('member.update',['id'=>$user->id])}}"
            @else
                action="{{route('member.store')}}"
            @endif>
                @csrf

                @if($user)
                    <div class="form-group">
                        <label for="name">Membership Number</label>
                        <input type="text" class="form-control" name="member_no" value="{{$user->member_no}}" required>
                    </div>
                    @error('member_no')
                        <small id="nameHelp" class="t-danger">{{$message}}</small>
                    @enderror
                @else
                    @php
                        $max = App\Member::where('id','<>',0)->max('id');
                        $max = $max?'N'.date('Y').'-'.$max:'N'.date('Y').'-1';
                    @endphp
                    <div class="form-group">
                        <label for="member_no">Membership Number</label>
                        <input type="text" class="form-control" name="member_no" value="{{old('member_no')?old('member_no'):$max}}" required>
                    </div>
                    @error('member_no')
                        <small id="nameHelp" class="t-danger">{{$message}}</small>
                    @enderror
                @endif
                
                <div class="form-group">
                    <label for="join_date">Joining Date</label>
                    <input type="text" class="form-control" name="join_date" id="join_date" @if($user)value="{{Carbon\Carbon::parse($user->join_date)->isoFormat('DD/MM/YYYY')}}" @else value="{{old('join_date')}}" @endif placeholder="dd/mm/yyyy" required >
                </div>

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name" @if($user) value="{{$user->name}}" @else value="{{old('name')}}" @endif placeholder="Enter name" required>
                    @error('name')
                        <small id="nameHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="father_name">Father/Husband Name</label>
                    <input type="text" class="form-control" name="father_name" id="father_name" @if($user) value="{{$user->father_name}}" @else value="{{old('father_name')}}" @endif placeholder="Enter Father/Husband name" required>
                    @error('father_name')
                        <small id="nameHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" @if($user) value="{{$user->email}}" @else value="{{old('email')}}" @endif placeholder="Enter email">
                    @error('email')
                        <small id="emailHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="cnic">CNIC</label>
                    <input type="text" class="form-control" name="cnic" id="cnic" @if($user) value="{{$user->cnic}}" @else value="{{old('cnic')}}" @endif placeholder="Enter cnic">
                    @error('cnic')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="mobile">Mobile Number</label>
                    <input type="text" class="form-control" name="mobile" id="mobile" @if($user) value="{{$user->mobile}}" @else value="{{old('mobile')}}" @endif placeholder="Enter mobile" required>
                    @error('mobile')
                        <small id="mobileHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="phone">Phone Number</label>
                    <input type="text" class="form-control" name="phone" id="phone" @if($user) value="{{$user->phone}}" @else value="{{old('phone')}}" @endif placeholder="Enter phone">
                    @error('phone')
                        <small id="phoneHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="role">Gender</label>
                    <select class="form-control" id="gender" name="gender" required>

                        <option value=""> Select </option>
                        <option value="male" @if( $user && $user->gender == 'male' ) selected @elseif( old('gender') && old('gender') == 'male' ) selected @endif> Male </option>
                        <option value="female" @if( $user && $user->gender == 'female' ) selected @elseif( old('gender') && old('gender') == 'female' ) selected @endif> Female </option>
                        <option value="other" @if( $user && $user->gender == 'other' ) selected @elseif( old('gender') && old('gender') == 'other' ) selected @endif> Other </option>

                    </select>
                    @error('gender')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="timing">Timing</label>
                    {{-- <input type="text" class="form-control" name="timing" id="timing" @if($user) value="{{$user->timing}}" @else value="{{old('timing')}}" @endif placeholder="Enter timing" required> --}}
                    <select class="form-control" id="timing" name="timing" required>

                        <option value=""> Select </option>
                        <option value="morning" @if( $user && $user->timing == 'morning' ) selected @elseif( old('timing') && old('timing') == 'morning' ) selected @endif> Morning </option>
                        <option value="evening" @if( $user && $user->timing == 'evening' ) selected @elseif( old('gender') && old('timing') == 'evening' ) selected @endif> Evening </option>
                        <option value="night" @if( $user && $user->timing == 'night' ) selected @elseif( old('timing') && old('timing') == 'night' ) selected @endif> Night </option>

                    </select>
                    @error('timing')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="age">Age</label>
                    <input type="number" class="form-control" name="age" id="age" @if($user) value="{{$user->age}}" @else value="{{old('age')}}" @endif placeholder="Enter age" required>
                    @error('age')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="height">Height</label>
                    <input type="text" class="form-control" name="height" id="height" @if($user) value="{{$user->height}}" @else value="{{old('height')}}" @endif placeholder="Enter height">
                    @error('height')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="weight">Weight</label>
                    <input type="text" class="form-control" name="weight" id="weight" @if($user) value="{{$user->weight}}" @else value="{{old('weight')}}" @endif placeholder="Enter weight">
                    @error('weight')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="role">Occupation</label>
                    <select class="form-control" id="occupation" name="occupation">

                        <option value=""> Select </option>
                        <option value="student" @if( $user && $user->occupation == 'student' ) selected @elseif( old('occupation') && old('occupation') == 'student' ) selected @endif> Student </option>
                        <option value="employed" @if( $user && $user->occupation == 'employed' ) selected @elseif( old('occupation') && old('occupation') == 'employed' ) selected @endif> Employed </option>
                        <option value="business" @if( $user && $user->occupation == 'business' ) selected @elseif( old('occupation') && old('occupation') == 'business' ) selected @endif> Business </option>
                        <option value="house wife" @if( $user && $user->occupation == 'house wife' ) selected @elseif( old('occupation') && old('occupation') == 'house wife' ) selected @endif> House Wife </option>

                    </select>
                    @error('occupation')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="weight">Residence Address</label>
                    <textarea name="address" id="address" class="form-control" required>@if($user){{$user->address}}@else{{old('address')}}@endif</textarea>
                    @error('address')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="medical_problem">Medical Problem (E.g Blood Pressure, Sugar etc)</label>
                    <textarea name="medical_problem" id="medical_problem" class="form-control" required>@if($user){{$user->medical_problem}}@else{{old('medical_problem')}}@endif</textarea>
                    @error('medical_problem')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="package">Package (Rs.)</label>
                    <input type="number" min="1" class="form-control" name="package" id="package" @if($user) value="{{$user->package}}" @else value="{{old('package')?old('package'):0}}" @endif placeholder="Enter package" required>
                    <input type="hidden" name="package_id" id="package_id" @if($user) value="{{$user->package_id}}" @else value="{{old('package_id')?old('package_id'):''}}" @endif >
                    @error('package')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="branch">Branch</label>
                    <input type="text" class="form-control" name="branch" id="branch" @if($user) value="{{$user->branch}}" @else value="{{old('branch')}}" @endif placeholder="Enter branch">
                    @error('branch')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="checkbox checkbox-primary pl-3">
                        <input type="checkbox" class="control-input" id="status" name="status" @if( $user && $user->status == 1 ) checked @elseif( !$user ) checked @endif >
                        <label for="status">
                            Enable
                        </label>
                    </div>
                </div>
                
                <input type="submit" class="btn btn-primary" value="Submit" >
            </form>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card-boxx">

            @php
                $groups = App\Package::where('status',1)->orderby('id','ASC')->get();
                $data = [];
                foreach( $groups as $item ){
                    $data[$item->type][] = $item;
                }

                if( $user ){
                    $package_id = explode(',',$user->package_id);
                } else {
                    if( old('package_id') ){
                        $package_id = explode(',',old('package_id'));
                    } else {
                        $package_id = [];
                    }
                }
            @endphp

            @foreach ($data as $type=>$items)
                <h4 class="mt-0 mb-3 header-title">
                    {{$type}}
                </h4>
                <div class="row">
                    
                    @foreach ($items as $item)
                
                        <div class="col-xl-3 col-md-4 col-sm-4 col-xs-6 package_box @if( in_array($item->id,$package_id) ) remove_package selected_box @else add_package @endif" data-id="{{$item->id}}" data-amount="{{$item->amount}}">
                            <div class="card-box widget-user">
                                <div class="text-center">
                                    <h2 class="font-weight-normal text-primary" >Rs.{{$item->amount}}</h2>
                                    <h5>{{$item->name}}</h5>
                                </div>
                            </div>
                        </div>

                    @endforeach

                </div>
                <!-- end row -->

            @endforeach


        </div>
    </div>

</div>


@endsection


@push('scripts')

<script>

$(document).ready(function(){
        
    var mask_format = {
        translation: {
            'D': {pattern: /[0]/, optional: false},
            'M': {pattern: /[3]/, optional: false},
            'M': {pattern: /[3]/, optional: false},
        }
    }
    $('#join_date').mask('00/00/0000');
    
    $( "#join_date" ).datepicker();
    $( "#join_date" ).datepicker( "option", "dateFormat", "dd/mm/yy" ); 

    @if($user)
        $('#join_date').val("{{Carbon\Carbon::parse($user->join_date)->isoFormat('DD/MM/YYYY')}}");
    @endif
        
});

$(document).ready(function(){

    function setPackageId(){
        let ids = '';
        $('.remove_package').each(function(i,v){
            ids += ( $(v).attr('data-id') + ',' );
        });
        $('#package_id').val(ids);
    }

    $(document).on('click','.add_package', function(){
        let id = $(this).attr('data-id');
        let amount = parseInt($(this).attr('data-amount'));

        let package_amount = parseInt($('#package').val());
        if( $('#package').val() == '' ){
            package_amount = 0;
        }

        $(this).addClass('remove_package');
        $(this).addClass('selected_box');
        $(this).removeClass('add_package');

        $('#package').val(package_amount+amount);
        setPackageId();
    });

    $(document).on('click','.remove_package', function(){
        let id = $(this).attr('data-id');
        let amount = parseInt($(this).attr('data-amount'));
        let package_amount = parseInt($('#package').val());

        $(this).addClass('add_package');
        $(this).removeClass('selected_box');
        $(this).removeClass('remove_package');

        let cal = package_amount-amount;
        if( cal < 0 ){
            $('#package').val(0);
        } else {
            $('#package').val(cal);
        }
        setPackageId();

    });

});

</script>


@endpush