@extends('layouts.admin.app')

@push('css')
<!-- third party css -->
<link href="{{asset('admin/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />

<link href="{{asset('admin/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />

{{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}

<!-- third party css end -->
<style>
  td.not_paid {
    background: #ff4f4fa6;
  }
  td.paid {
    background: #10c469c2;
    color: #fff;
  }
  .text_dim {
    color: #6c757d;
  }
  
/*input[type="date"]::-webkit-datetime-edit-text { color: transparent;  }*/

/*input[type="date"]::-webkit-datetime-edit, input[type="date"]::-webkit-inner-spin-button, input[type="date"]::-webkit-clear-button {*/
/*  color: #fff;*/
/*  position: relative;*/
/*}*/

/*input[type="date"]::-webkit-datetime-edit-year-field{*/
/*  position: absolute !important;*/
/*  border-left:1px solid #8c8c8c;*/
/*  padding: 2px;*/
/*  color:#fff;*/
/*  left: 56px;*/
/*}*/

/*input[type="date"]::-webkit-datetime-edit-month-field{*/
/*  position: absolute !important;*/
/*  border-left:1px solid #8c8c8c;*/
/*  padding: 2px;*/
/*  color:#fff;*/
/*  left: 26px;*/
/*}*/


/*input[type="date"]::-webkit-datetime-edit-day-field{*/
/*  position: absolute !important;*/
/*  color:#fff;*/
/*  padding: 2px;*/
/*  left: 4px;*/
  
/*}*/
</style>

@endpush
@section('page_header') Due Date Report @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">

              <form action="" method="GET">

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                        <label for="users">Date</label>
                        <input type="text" class="form-control" name="date_from" id="date_from" value="{{$date_from_formatted}}" placeholder="dd/mm/yyyy" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                        <label style="color:#ffffff00">Action</label> <br>
                        <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                </div>

                {{-- <div class="row">
                  <div class="col-md-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div> --}}

              </form>

              @if( count($records) != 0 )
              <table id="adminlistTable"  class="table table-bordered dt-responsivex nowrap" style="width:100%;">
                  <thead>
                      <tr>
                          <th width="50px">Member#</th>
                          <th width="50px">Name (Joining) </th>
                          <th width="50px">F.Name</th>
                          <th width="50px">Mobile</th>
                          <th width="50px">Amount</th>
                          <th width="50px">Due Date</th>
                      </tr>
                  </thead>
                  <tbody>

                    @foreach($records as $key=>$record)
                    <tr>
                      <td>{{$record->member_no}}</td>
                      <td>{{ucfirst($record->name)}} ({{$record->m_join_date}})</td>
                      <td>{{ucfirst($record->father_name)}}</td>
                      <td>{{$record->mobile}}</td>
                      <td>Rs {{$record->package}}</td>
                      <td>{{$due_date}}</td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
              @else 

                @if( $date_from && ($date_from != '') )
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-center">
                                No Record Found!
                            </h4>
                        </div>
                    </div>
                @endif

              @endif

            </div>

        </div>
    </div>


@endsection

@push('scripts')
<!-- third party js -->
<script src="{{asset('admin/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/responsive.bootstrap4.min.js')}}"></script>


<script src="{{asset('admin/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{asset('admin/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('admin/libs/pdfmake/vfs_fonts.js')}}"></script>

<!-- third party js ends -->
<script>
    
    $(document).ready(function(){
        
        var mask_format = {
            translation: {
                'D': {pattern: /[0]/, optional: false},
                'M': {pattern: /[3]/, optional: false},
                'M': {pattern: /[3]/, optional: false},
            }
        }
        
        $('#date_from').mask('00/00/0000');
        
        $( "#date_from" ).datepicker();
        $( "#date_from" ).datepicker( "option", "dateFormat", "dd/mm/yy" ); 
        
        $('#date_from').val("{{$date_from_formatted}}");
        
    });
      
  $('#adminlistTable').DataTable({
    "dom": 'Bfrtip',
        "buttons": [
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
    "scrollX": true,
    "aaSorting": [],
    "ordering": false,
    "lengthMenu": [[-1], ["All"]],
    "drawCallback": function( settings ) {
      //   initShowDetailsMethod();
    } //end drawcallback
  });

</script>
@endpush
