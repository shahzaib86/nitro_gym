@extends('layouts.admin.app')

@push('css')
<!-- third party css -->
<link href="{{asset('admin/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
<style>
  td.not_paid {
    background: #ff4f4fa6;
  }
  td.paid {
    background: #10c469c2;
    color: #fff;
  }
  .text_dim {
    color: #6c757d;
  }
</style>

@endpush
@section('page_header') Financial Report @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            
            <div class="card-box table-responsive">

              <form action="" method="GET">

                <div class="row">

                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="users">Date From</label>
                      <input type="date" class="form-control" name="date_from" id="date_from" value="{{$date_from}}" />
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="users">Date To</label>
                      <input type="date" class="form-control" name="date_to" id="date_to" value="{{$date_to}}" />
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label style="color:#ffffff00">Action</label> <br>
                      <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                  </div>

                </div>

                {{-- <div class="row">
                  <div class="col-md-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div> --}}

              </form>

                @if( count($record) != 0 )
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <a href="{{route('admin.home')}}">
                                <div class="card-box widget-user" style="background:#3a4250;">
                                    <div class="text-center">
                                        <h2 class="font-weight-normal text-success">Rs. {{number_format($record[0]->total)}}  </h2>
                                        <h5>Total Fees Amount</h5>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-md-6">
                          <a href="{{route('admin.home')}}">
                              <div class="card-box widget-user" style="background:#3a4250;">
                                  <div class="text-center">
                                      <h2 class="font-weight-normal text-danger">Rs. {{number_format($expense[0]->total)}}  </h2>
                                      <h5>Total Expenses</h5>
                                  </div>
                              </div>
                          </a>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <a href="{{route('admin.home')}}">
                                <div class="card-box widget-user" style="background:#3a4250;">
                                    <div class="text-center">
                                        <h2 class="font-weight-normal text-primary"> {{$record[0]->count}}  </h2>
                                        <h5>Total Fee Slips</h5>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @elseif( isset($date_from) && isset($date_to) )

                @endif

            </div>

        </div>
    </div>


@endsection

@push('scripts')
<!-- third party js -->
<script src="{{asset('admin/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<!-- third party js ends -->
<script>

      
//   $('#adminlistTable').DataTable({
//     "scrollX": true,
//     "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
//     "pageLength": 50,
//     "drawCallback": function( settings ) {
//       //   initShowDetailsMethod();
//     } //end drawcallback
//   });

</script>
@endpush
