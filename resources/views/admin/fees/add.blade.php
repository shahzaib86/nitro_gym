@extends('layouts.admin.app',['title'=>'Fees - ' ])

@push('css')
{{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}
<style>
    .select2-selection--single {
        height: 36px !important;
        border: 1px solid #ced4da !important;
    }
    .select2-selection__rendered {
        height: 36px !important;
        line-height: 36px !important;
        color: unset !important;
        padding: 0rem .8em !important;
    }

    .add_package,
    .remove_package {
        cursor: pointer;
    }

    .selected_box .widget-user {
        background: #caff00;
    }

    .selected_box .text-primary {
        color: #606060!important;
    }

    .selected_box h5{
        color: #5a5a5a !important;
    }
    .select2-results__options {
        color:#000;
    }
    .select2-container--default .select2-results__option[aria-selected=true] {
        background-color: #3a4250 !important;
        color: #e1e9ee !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #444 !important;
    }

/* input[type="date"]::-webkit-datetime-edit-text { color: transparent;  }

input[type="date"]::-webkit-datetime-edit, input[type="date"]::-webkit-inner-spin-button, input[type="date"]::-webkit-clear-button {
  color: #fff;
  position: relative;
}

input[type="date"]::-webkit-datetime-edit-year-field{
  position: absolute !important;
  border-left:1px solid #8c8c8c;
  padding: 2px;
  color:#fff;
  left: 56px;
}

input[type="date"]::-webkit-datetime-edit-month-field{
  position: absolute !important;
  border-left:1px solid #8c8c8c;
  padding: 2px;
  color:#fff;
  left: 26px;
}


input[type="date"]::-webkit-datetime-edit-day-field{
  position: absolute !important;
  color:#fff;
  padding: 2px;
  left: 4px;
  
} */

</style>

@endpush
@section('page_header') Add Fees @endsection
@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card-box">

            <h4 class="mt-0 mb-3 header-title">
                Add Fees
            </h4>

            <form role="form" method="POST" action="{{route('fees.store')}}" >
                @csrf
                <div class="form-group">
                    <label for="member">Member</label>
                    <select class="form-control select2" id="member" name="member" required>

                        <option value=""> Select </option>
                        @foreach( App\Member::getActive() as $item )
                            <option value="{{$item->id}}">{{$item->name}} ({{$item->member_no}}) (Joined on: {{$item->m_join_date}}) {{$item->getLastSubmittedFees()}}</option>
                        @endforeach

                    </select>
                    @error('member')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label for="name">Date</label>
                    <input type="text" class="form-control" name="date" id="fee_date" placeholder="dd/mm/yyyy" required>
                    @error('date')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="name">Amount</label>
                    <input type="number" min="1" class="form-control" name="amount" id="amount" placeholder="Enter amount" value="0" required>
                    <input type="hidden" name="package_id" id="package_id" value="{{old('package_id')?old('package_id'):''}}"  >
                    @error('amount')
                        <small class="t-danger">{{$message}}</small>
                    @enderror
                </div>
                
                <input type="submit" class="btn btn-primary" value="Submit" >
            </form>
        </div>
    </div>


    <div class="col-md-6">
        <div class="card-boxx">

            @php
                $groups = App\Package::where('status',1)->orderby('id','ASC')->get();
                $data = [];
                foreach( $groups as $item ){
                    $data[$item->type][] = $item;
                }

                if( old('package_id') ){

                }

            @endphp

            @foreach ($data as $type=>$items)
                <h4 class="mt-0 mb-3 header-title">
                    {{$type}}
                </h4>
                <div class="row">
                    
                    @foreach ($items as $item)
                
                        <div class="col-xl-3 col-md-4 col-sm-4 col-xs-6 package_box add_package" data-id="{{$item->id}}" data-amount="{{$item->amount}}">
                            <div class="card-box widget-user">
                                <div class="text-center">
                                    <h2 class="font-weight-normal text-primary" >Rs.{{$item->amount}}</h2>
                                    <h5>{{$item->name}}</h5>
                                </div>
                            </div>
                        </div>

                    @endforeach

                </div>
                <!-- end row -->

            @endforeach


        </div>
    </div>


</div>


@endsection


@push('scripts')
{{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script> --}}
<script>

$(document).ready(function(){
        
    var mask_format = {
        translation: {
            'D': {pattern: /[0]/, optional: false},
            'M': {pattern: /[3]/, optional: false},
            'M': {pattern: /[3]/, optional: false},
        }
    }
    
    $('#fee_date').mask('00/00/0000');
    
    $( "#fee_date" ).datepicker();
    $( "#fee_date" ).datepicker( "option", "dateFormat", "dd/mm/yy" ); 
        
});


$(document).ready(function(){

    $('.select2').select2();

    function setPackageId(){
        let ids = '';
        $('.remove_package').each(function(i,v){
            ids += ( $(v).attr('data-id') + ',' );
        });
        $('#package_id').val(ids);
    }

    $(document).on('click','.add_package', function(){
        let id = $(this).attr('data-id');
        let amount = parseInt($(this).attr('data-amount'));

        let package_amount = parseInt($('#amount').val());
        if( $('#amount').val() == '' ){
            package_amount = 0;
        }

        $(this).addClass('remove_package');
        $(this).addClass('selected_box');
        $(this).removeClass('add_package');

        $('#amount').val(package_amount+amount);
        setPackageId();
    });

    $(document).on('click','.remove_package', function(){
        let id = $(this).attr('data-id');
        let amount = parseInt($(this).attr('data-amount'));
        let package_amount = parseInt($('#amount').val());

        $(this).addClass('add_package');
        $(this).removeClass('selected_box');
        $(this).removeClass('remove_package');

        let cal = package_amount-amount;
        if( cal < 0 ){
            $('#amount').val(0);
        } else {
            $('#amount').val(cal);
        }
        setPackageId();

    });

});

</script>

@endpush