@extends('layouts.admin.app')

@push('css')
<!-- third party css -->
<link href="{{asset('admin/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
<style>
  td.not_paid {
    background: #ff4f4fa6;
  }
  td.paid {
    background: #10c469c2;
    color: #fff;
  }
  .text_dim {
    color: #6c757d;
  }
</style>
@endpush
@section('page_header') Fees Report @endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card-box" style="max-width: 600px;">
            <!-- <div class="panel-heading">
                <h4>Invoice</h4>
            </div> -->
            <div class="panel-body">
                <div class="clearfix">
                    <div class="float-left">
                        
                        <img src="{{asset('admin/images/nitrogen-light.png')}}" alt="" height="30">
                        {{-- <h3>Adminto</h3> --}}
                        @php $setting = App\Boiler\Setting::getValues(['address','phone']); @endphp
                        <address style="margin-top:10px;margin-bottom:0px;">
                            {{$setting['address']}}<br>
                            Phone {{$setting['phone']}}
                        </address>

                    </div>
                    <div class="float-right">
                        <img src="{{asset('admin/images/nitrogen-light.png')}}" alt="" style="opacity: 0;" height="30">
                        <h4>Receipt# 
                            <strong> {{$fees->id}} </strong>
                        </h4>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">

                        <div class="float-left mt-0" style="font-size: 16px;">
                            <p><strong>Membership#</strong> {{$fees->member->member_no}}</p>
                            <p><strong>Mr./Mrs./Miss.</strong> {{$fees->member->name}}</p>
                            <p><strong>Date: </strong> {{$fees->m_date}}</p>
                            {{-- <address>
                                <strong>Membership#</strong> N2021-1 <br>
                                <strong>Mr./Mrs./Miss.</strong> Ejaz Ismail<br>
                                <strong>Joining Date</strong> 10 Jan, 2021<br>
                            </address> --}}
                        </div>
                        
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                @php 
                    $groups = App\Package::where('status',1)->orderby('id','ASC')->get();
                    $data = [];
                    foreach( $groups as $item ){
                        $data[$item->type][] = $item;
                    }

                    if( $fees ){
                        $package_id = explode(',',$fees->package_id);
                    } else {
                        $package_id = [];
                    }
                @endphp

                @if( count($package_id) != 0 )
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table mt-1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Charges</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($package_id as $key=>$item)
                                    @php 
                                        $package = App\Package::find($item);
                                    @endphp
                                    @if( $package )
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$package->name}}</td>
                                        <td>{{$package->amount}}</td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif

                <div class="row">
                    {{-- <div class="col-xl-6 col-6">
                        <div class="clearfix mt-4">
                            <h5 class="small text-dark">PAYMENT TERMS AND POLICIES</h5>

                            <small>
                                All accounts are to be paid within 7 days from receipt of
                                invoice. To be paid by cheque or credit card or direct payment
                                online. If account is not paid within 7 days the credits details
                                supplied as confirmation of work undertaken will be charged the
                                agreed quoted fee noted above.
                            </small>
                        </div>
                    </div> --}}
                    <div class="col-xl-12 col-12 offset-xl-3x" >
                        {{-- <p class="text-right"><b>Sub-total:</b> 2930.00</p>
                        <p class="text-right">Discout: 12.9%</p>
                        <p class="text-right">VAT: 12.9%</p> --}}
                        <hr>
                        <h3 class="text-right">Total: Rs. {{$fees->amount}}</h3>
                    </div>
                </div>
                <hr>
                <div class="d-print-none">
                    <div class="float-right">
                        <a href="javascript:window.print()" class="btn btn-dark waves-effect waves-light"><i class="fa fa-print"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- end row --> 

@endsection

@push('scripts')
<script>

</script>
@endpush
