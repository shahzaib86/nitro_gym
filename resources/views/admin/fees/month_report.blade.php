@extends('layouts.admin.app')

@push('css')
<!-- third party css -->
<link href="{{asset('admin/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
<style>
  td.not_paid {
    background: #ff4f4fa6;
  }
  td.paid {
    background: #10c469c2;
    color: #fff;
  }
  .text_dim {
    color: #6c757d;
  }
</style>

@endpush
@section('page_header') Fees Report @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <a href="{{ route('fees.add') }}" class="my-2 btn btn-success">
                Add Fees <i class="fa fa-plus"></i>
            </a>
            <div class="card-box table-responsive">

              <form action="" method="GET">

                <div class="row">

                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="users">Date From</label>
                      <input type="date" class="form-control" name="date_from" id="date_from" value="{{$date_from}}" />
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="users">Date To</label>
                      <input type="date" class="form-control" name="date_to" id="date_to" value="{{$date_to}}" />
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label style="color:#ffffff00">Action</label> <br>
                      <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                  </div>

                </div>

                {{-- <div class="row">
                  <div class="col-md-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div> --}}

              </form>

              @if( count($records) != 0 )
              <table id="adminlistTable"  class="table table-bordered dt-responsivex nowrap" style="width:100%;">
                  <thead>
                      <tr>
                          <th width="50px">Member#</th>
                          <th width="100px">Name (Joining)</th>
                          @foreach ($records as $key=>$item)
                            <th>{{$key}}</th>
                          @endforeach
                      </tr>
                  </thead>
                  <tbody>

                    @foreach($members as $key=>$member)
                    <tr>
                      <td>{{$member->member_no}}</td>
                      <td> {{$member->name}} <span class="text_dim">({{$member->m_join_date}})</span> </td>
                      
                      @foreach ($records as $key=>$item)
                        @if( isset($item['data'][$member->member_no]) )
                          <td class="paid"> Rs. {{ $item['data'][$member->member_no] }} </td>
                        @else
                          <td class="not_paid">  </td>
                        @endif
                      @endforeach
                      
                    </tr>
                    @endforeach
                  </tbody>
              </table>
              @endif

            </div>

        </div>
    </div>


@endsection

@push('scripts')
<!-- third party js -->
<script src="{{asset('admin/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<!-- third party js ends -->
<script>

      
  $('#adminlistTable').DataTable({
    "scrollX": true,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "pageLength": 50,
    "drawCallback": function( settings ) {
      //   initShowDetailsMethod();
    } //end drawcallback
  });

</script>
@endpush
