@extends('layouts.admin.app')
@section('page_header') Add Page @endsection
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                <form method="post" enctype="multipart/form-data">
                        @csrf
                        <!-- text input -->
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" id="title" name="title" class="form-control"
                                {{-- value="{{ $emailtemplate->type }}"  --}}
                                placeholder="title">
                            @error('title')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="keyword">Keyword</label>
                            <input type="text" id="keyword" name="keyword" class="form-control"
                                {{-- value="{{ $emailtemplate->type }}"  --}}
                                placeholder="keyword">
                            @error('keyword')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" id="description" name="description" class="form-control"
                                {{-- value="{{ $emailtemplate->type }}"  --}}
                                placeholder="description">
                            @error('description')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="body">Body</label>
                            <textarea type="text" id="body" name="body" class="form-control summernote"></textarea>
                            @error('body')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-info pull-right">Save</button>

                </div>
                </form>
            </div>
        </div>
    </div>

@endsection