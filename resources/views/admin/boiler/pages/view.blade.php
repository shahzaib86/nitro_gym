@extends('layouts.admin.app')
@section('page_header') Pages @endsection
@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            {{-- {{ route('food_type-add') }} --}}
        <a href="{{route('pages.add')}}" class="my-2 btn btn-success">
                Add New
                <i class="fa fa-plus"></i>
            </a>
            <div class="card-box table-responsive">
                <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>slug</th>
                            <th>Keyword</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($Pages as $page)
                        <tr>
                            <td>{{ Str::limit($page->title, 25) }}</td>
                            <td>{{ Str::limit($page->slug, 25) }}</td>
                            <td>{{ Str::limit($page->keyword, 25) }}</td>
                            <td>{{ Str::limit($page->description, 25) }}</td>
                            <td>
                                {{-- {{ route('email.template.edit', ['id'=>$emailtemplate->id])}} --}}
                                <a class="btn btn-info btn-sm text-white cursor-pointer" href="{{ route('pages.edit',['id'=>$page->id])}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                {{-- <a href="{{ route('pages.delete',['id'=>$page->id])}}"
                                    class="delete_user btn btn-danger " data-user_id=""> Delete</a> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- container-fluid -->

</div>
<!-- content -->
@endsection
