@extends('layouts.admin.app')

@push('css')
<!-- third party css -->
<link href="{{asset('admin/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endpush
@section('page_header') Activity Logs @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            {{-- <a href="{{ route('food_item-add') }}" class="my-2 btn btn-success">
                Add New
                <i class="fa fa-plus"></i>
            </a> --}}
            <div class="card-box table-responsive">

                <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                       <label for="users">Users</label>
                                    <select class="select2" id="users" name="users[]" multiple="multiple">
                                            <option value=""> Select to search </option>
                                            @foreach(App\user::all() as $data)
                                              <option value="{{$data->id}}">{{$data->name}}</option>
                                            @endforeach
                                    </select>
                    </div>
                </div>
                
            </div>

                <table id="activitylogsTable"  class="table table-bordered dt-responsive nowrap" style="width:100%;">
                    <thead>
                        <tr>
                            <th width="10px">#</th>
                            <th>User</th>
                            <th>Text</th>
                            <th>TimeStamp</th>
                            <th>Params</th>

                        </tr>
                    </thead>
                    <tbody>
                        {{-- @php $i = 0 @endphp
                        @foreach ($result as $result)
                            @php $i++ @endphp
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $result->name }} </td>
                                <td>
                                    <a href="{{ route('food_item-edit', [$result->id]) }}" class="btn btn-info ">
                                        Edit </a>
                                    <a href="{{ route('food_item-delete', [$result->id]) }}"
                                        class="delete_user btn btn-danger " data-user_id=""> Delete</a>
                                </td>
                            </tr>
                        @endforeach --}}
                    </tbody>
                </table>
            </div>

        </div>
    </div>




@endsection


@push('scripts')
<!-- third party js -->
<script src="{{asset('admin/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<!-- third party js ends -->
<script>


search_start();

$('#users').change(function(){
    search_start();
})


function search_start(){
      if ($.fn.DataTable.isDataTable("#activitylogsTable")) $("#activitylogsTable").DataTable().destroy();
      //Start getting filters

      let users = $('#users').val();
      
      $('#activitylogsTable').DataTable({
        "scrollX": true,
       processing: true,
       serverSide: true,
       ajax: {
            url: "{{route('activitylogs.fetch')}}",
            type: "POST",
            data: {
              "_token": "{{ csrf_token() }}",
              users:users,
            }
        },
       createdRow: function( row, data, dataIndex ) {
            // $(row).attr('class', 'data-row click_detail');
            // $(row).attr('data-id', data.id);
            // console.log(row,data);
        },
       columns: [
                { data: 'DT_RowIndex', user: 'DT_RowIndex' },
                { data: 'user' },
                { data: 'text' },
                { data: 'time' },
                { data: 'params' },
                               
              ],
              "drawCallback": function( settings ) {
                //   $('#search-result-div').show();
                //   $('#search-overlay').hide();
                //   initShowDetailsMethod();
              } //end drawcallback
    });
}


            $(document).ready(function() {
                $('.select2').select2();
            });

</script>
@endpush
