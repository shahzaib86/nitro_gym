@extends('layouts.admin.app',['title'=>'Dashboard - ', 'page_head'=>'Dashboard' ])

@push('css')
<style>
  td.not_paid {
    background: #ff4f4fa6;
    color: #fff;
    text-align: center;
  }
  td.paid {
    background: #10c469c2;
    color: #fff;
    text-align: center;
  }
  td.no_fee {
    background: #fff;
    color: #fff;
    text-align: center;
  }
  .text_dim {
    color: #6c757d;
  }
  .fee_check {
    font-size: 20px;
    font-weight: bold;
  }
  #adminlistTable_filter {
    float: left;
  }
</style>
@endpush

@section('page_header') Dashboard @endsection
@section('content')

<div class="row">
    <div class="col-xl-3 col-md-6">
        <a href="{{route('admin.home')}}">
            <div class="card-box widget-user">
                <div class="text-center">
                    <h2 class="font-weight-normal text-danger" data-plugin="counterup"> {{App\Member::count()}} </h2>
                    <h5>Total Members</h5>
                </div>
            </div>
        </a>
    </div>
    <div class="col-xl-3 col-md-6">
        <a href="{{route('admin.home')}}">
            <div class="card-box widget-user">
                <div class="text-center">
                    <h2 class="font-weight-normal text-primary" data-plugin="counterup"> {{App\Member::where('status',1)->count()}} </h2>
                    <h5>Active Members</h5>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        
        <h4 class="text-capitalize">Due Date Sheet</h4>
        <div class="card-box table-responsive">
          <form action="" method="GET">

            <div class="row">

              {{-- <div class="col-md-3">
                <div class="form-group">
                  <label for="users">Date From</label>
                  <input type="date" class="form-control" name="date_from" id="date_from" />
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label for="users">Date To</label>
                  <input type="date" class="form-control" name="date_to" id="date_to" />
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label style="color:#ffffff00">Action</label> <br>
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>

            </div> --}}

          </form>

          <table id="adminlistTable"  class="table table-bordered dt-responsivex nowrap" style="width:100%;">
              <thead>
                  <tr>
                    <th width="150px">Member#</th>
                    <th width="400px">Name (Joining)</th>
                    @foreach ($records as $key=>$item)
                        <th width="200px">Due Date ({{$key}})</th>
                    @endforeach
                    <th >Action</th>

                  </tr>
              </thead>
              <tbody>
                @php
                  $packages = App\Package::all();
                  $packages_arr = [];
                  foreach($packages as $item){
                    $packages_arr['package_'.$item->id] = $item->name;
                  }
                @endphp

                @foreach($members as $key=>$member)
                    @php 
                        $due_day = Carbon\Carbon::parse($member->join_date)->isoFormat('DD');
                        $today_date = Carbon\Carbon::parse($today)->isoFormat('-MM-YYYY');
                        $today_date_carbon = Carbon\Carbon::parse($today)->isoFormat('YYYY-MM-DD');
                        // $due_date = $due_day.$today_date;
                        $due_date = $due_day;
                        // $due_date_carbon = Carbon\Carbon::parse($due_date)->isoFormat('YYYY-MM-DD');
                        
                        $join_date_carbon = Carbon\Carbon::parse($member->join_date)->isoFormat('YYYY-MM-DD');
                        
                    @endphp
                    <tr>
                      <td>{{$member->member_no}}</td>
                      <td> {{$member->name}} <span class="text_dim">({{$member->m_join_date}}) </span> <br>
                        Phone: {{$member->mobile}} <br>
                        Package: Rs {{$member->package}} <br>
                        @php 
                          $ex_pack = explode(',',$member->package_id);
                          foreach( $ex_pack as $item ){
                            if( isset($packages_arr['package_'.$item]) ) {
                              echo $packages_arr['package_'.$item].',';
                            }
                          }
                        @endphp
                      </td>
                      
                      @foreach ($records as $key=>$item)
                        @php
                        $due_date = $due_day.'-'.$item['month'].'-'.$item['year'];
                        $due_date_carbon = Carbon\Carbon::parse($due_date)->isoFormat('YYYY-MM-DD');
                        @endphp
                        
                        @if( isset($item['data'][$member->member_no]) )
                            <td class="paid" title="Rs. {{ $item['data'][$member->member_no] }}"> 
                              {{-- Rs. {{ $item['data'][$member->member_no] }}  --}}
                              {{-- <i class="mdi mdi-check fee_check"></i> --}}
                              {{$due_date}} 
                            </td>
                        @else
                            @php 
                                // $join_date = Carbon\Carbon
                            @endphp
                            
                            @if( $due_date_carbon < $join_date_carbon )
                                <td class="no_fee" title="Not Enrolled" > {{$due_date}} </td>
                            @else
                            
                                @if( $due_date_carbon >= $today_date_carbon )
                                    <td class="not_paidx" > {{$due_date}} </td>
                                @else
                                    <td class="not_paid"> {{$due_date}} </td>
                                @endif
                            
                            @endif

                        @endif
                      @endforeach
                      <td>
                        <a class="btn btn-info btn-sm text-white cursor-pointer" href="{{route('member.edit',['id'=>$member->id])}}">
                          <i class="fa fa-edit"></i>
                        </a>
                      </td>
                    </tr>
                @endforeach
                
              </tbody>
          </table>

        </div>

    </div>
</div>

@endsection

@push('scripts')

<!-- third party js -->
<script src="{{asset('admin/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<!-- third party js ends -->
<script>
  $('#adminlistTable').DataTable({
    dom: 'Bfrtip',
    "scrollX": true,
    "aaSorting": [],
    "ordering": false,
    "lengthMenu": [[-1], ["All"]],
    // "pageLength": 500,
    "drawCallback": function( settings ) {
      //   initShowDetailsMethod();
    } //end drawcallback
  });
</script>

@endpush