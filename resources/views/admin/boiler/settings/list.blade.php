@extends('layouts.admin.app',['title'=>'Configurations'])
@section('page_header') Configurations @endsection
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">


            <div class="card card-primary">
                {{-- <div class="card-header">
                    <h3 class="card-title">Attributes</h3>
                </div> --}}
                <!-- /.card-header -->
                <form action="{{route('admin.settings.update')}}" method="POST">
                    @csrf
                    {{-- <div class="card-body f-segoe ">
                <div class="row">
                  @foreach( $settings as $key=>$item )
                  <div class="row col-md-4  ml-1">
                    <div class="form-group report_no_box" >
                      <label> {{ ucfirst(str_replace('_',' ',$item->key)) }} </label>
                    <input type="text" class="form-control" name="{{$item->key}}" value="{{$item->value}}" required>
            </div>
        </div>
        @endforeach
    </div>

    </div> --}}

    <div class="card-body f-segoe ">
        <table class="table table-stripped table-hover">
            <thead>
                <tr>
                    <th width="30%">Title</th>
                    <th width="30%">Value</th>
                    <th width="40%">Description</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $settings as $key=>$item )
                <tr>
                    <td>{{ ($item->title)?$item->title:ucfirst(str_replace('_',' ',$item->key)) }} </td>
                    <td>
                        @if( $item->json_params && $item->json_params['type'] == "dropdown" )
                        <select class="form-control" name="{{$item->key}}">
                            @foreach ($item->json_params['data'] as $option)
                            <option value="{{$option['value']}}" @if( $item->value == $option['value'] ) selected @endif
                                > {{$option['label']}} </option>
                            @endforeach
                        </select>
                        @else
                        <input type="text" class="form-control" name="{{$item->key}}" value="{{$item->value}}" required>
                        @endif
                    </td>
                    <td> {{($item->description)?$item->description:'-'}} </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <button type="submit" class="btn btn-info pull-right">Update</button>

    </div>

    <!-- /.card-body -->


    </form>

    </div>
    <!-- /.card -->

    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection


@section('scripts')


<script>
    $(function () {
    
  });
</script>

@endsection