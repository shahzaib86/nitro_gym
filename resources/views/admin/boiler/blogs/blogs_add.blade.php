@extends('layouts.admin.app')

@section('page_header') Add Blog @endsection
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <form method="post" enctype="multipart/form-data">
                    @csrf
                    <!-- text input -->
                    <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-form-label">Title</label>
                            <input type="text" id="title" name="title" class="form-control"
                                {{-- value="{{ $emailtemplate->type }}" --}} placeholder="Title">
                            @error('title')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-form-label">Choose Image :</label>
                            <label class=" col-form-label" for="example-fileinput"></label>
                            <input type="file" class="form-control" id="example-fileinput" onchange="readURL(this);" name="file">
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-form-label">Select Category</label>
                            <select name="category" class="form-control" >
                                @foreach($categories as $allcat)
                            <option value="{{$allcat->id}}">{{$allcat->title}}</option>
                            @endforeach

                            </select>
                        </div>

                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="checkbox" name="status" id="status1" data-toggle="toggle" data-on="Enabled"
                                data-off="Disabled">
                            <label for="status1">Status</label>
                            @error('status')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="checkbox" name="featured" id="featured" data-toggle="toggle" data-on="featured"
                                data-off="Disabled">
                            <label for="featured">Featured</label>
                            @error('featured')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-form-label" for="template">Description</label>
                            <textarea type="text" id="description" name="description"
                                class="form-control summernote"></textarea>
                            @error('description')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                       
                    </div>
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                </form>
                <img id="Image" src="" style="width: 50%; margin-top:20px;" /> 
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#Image')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }


</script>
@endpush