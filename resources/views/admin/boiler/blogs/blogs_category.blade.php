@extends('layouts.admin.app')
@section('page_header') Blog Categories @endsection
@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            
            <a href="{{route('blogs_category.add')}}" class="my-2 btn btn-success">
                Add New
                <i class="fa fa-plus"></i>
            </a>
            <div class="card-box table-responsive">
                <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Sno.</th>
                            <th>Title</th>
                            <th>slug</th>
                            <th>Description</th>
                            <th width="10px">Featured</th>
                            <th width="10px">Status</th>
                            <th width="200px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($blogscategories as $key=> $blogscat)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{ $blogscat->title}}</td>
                            <td>{{ $blogscat->slug }}</td>
                            <td>{{ Str::limit($blogscat->description, 25) }}</td>
                            <td> @if ( $blogscat->featured == 1)
                                <span class="fas fa-star text-warning s-c"></span>
                                @else
                                <span class="far fa-star text-warning s-c"></span>
                                        @endif</td>
                            <td>
                                @if ( $blogscat->status == 1)
                                <span class="badge badge-success">Enable</span>
                                @else
                                <span class="badge badge-danger">Disabled</span>
                                @endif
                            </td>
                            <td>
                                {{--  --}}
                                <a class="btn btn-info btn-sm text-white cursor-pointer" href="{{ route('blogs_category.edit',['id'=>$blogscat->id])}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a class="btn btn-danger btn-sm text-white cursor-pointer" href="{{ route('blogs_category.delete',['id'=>$blogscat->id])}}">
                                    <i class="fa fa-trash"></i>
                                </a>
                                {{-- <a href="" class="delete_user btn btn-danger " data-user_id=""> Delete</a> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- container-fluid -->

</div>
<!-- content -->
@endsection