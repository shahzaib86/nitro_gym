@extends('layouts.admin.app')

@push('css')
<!-- third party css -->
<link href="{{asset('admin/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endpush
@section('page_header') Blogs @endsection
@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <a href="{{route('blogs.add')}}" class="my-2 btn btn-success">
                Add New
                <i class="fa fa-plus"></i>
            </a>
            <div class="card-box table-responsive">
                <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">Select Category</label>
                    <select id="category" class="form-control" >
                        <option value=""> Select to search </option>
                        @foreach(App\Boiler\BlogCategory::all() as $item)
                          <option value="{{$item->id}}">{{$item->title}}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="col-md-4">
              <div class="form-group">
                  <label class="col-form-label">Status</label>
                  <select id="blogs_status" class="form-control" >
                      <option value=""> Select to search </option>
                      {{-- @foreach(App\Models\FoodType::all() as $item) --}}
                        <option value="1">Enable</option>
                        <option value="0">Disable</option>
                      {{-- @endforeach --}}
                  </select>
              </div>
          </div>
          </div>

                <table id="blogsTable" class="table table-bordered dt-responsive nowrap" style="width:100%;">
                    <thead>
                        <tr>
                            <th width="10px">#</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Category</th>
                            <th width="10px">Featured</th>
                            <th width="10px">Status</th>
                            <th width="200px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- @foreach ($blogsdata as $key=> $blogs)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{ $blogs->title}}</td>
                            <td>{{ $blogs->slug }}</td>
                            <td>{{ Str::limit($blogs->description, 25) }}</td>
                            <td> @if ( $blogs->featured == 1)
                                <span class="fas fa-star text-warning s-c"></span>
                                @else
                                <span class="far fa-star text-warning s-c"></span>
                                        @endif</td>
                            <td>
                                @if ( $blogs->status == 1)
                                <span class="badge badge-success">Enable</span>
                                @else
                                <span class="badge badge-danger">Disabled</span>
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-info btn-sm text-white cursor-pointer" href="{{ route('blogs.edit',['id'=>$blogs->id])}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a class="btn btn-danger btn-sm text-white cursor-pointer" href="{{ route('blogs.delete',['id'=>$blogs->id])}}">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach --}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- container-fluid -->

</div>
<!-- content -->
@endsection

@push('scripts')
<!-- third party js -->
<script src="{{asset('admin/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<!-- third party js ends -->
<script>


search_start();

$('#category').change(function(){
    search_start();
})

$('#blogs_status').change(function(){
    search_start();
})

function search_start(){
      if ($.fn.DataTable.isDataTable("#blogsTable")) $("#blogsTable").DataTable().destroy();
      //Start getting filters

      let category = $('#category').val();
      let status = $('#blogs_status').val();
      
      $('#blogsTable').DataTable({
        "scrollX": true,
       processing: true,
       serverSide: true,
       ajax: {
            url: "{{route('blogs.fetch')}}",
            type: "POST",
            data: {
              "_token": "{{ csrf_token() }}",
              category:category,
              status,status,
            }
        },
       createdRow: function( row, data, dataIndex ) {
            // $(row).attr('class', 'data-row click_detail');
            // $(row).attr('data-id', data.id);
            // console.log(row,data);
        },
       columns: [
                { data: 'id' },
                { data: 'title' },
                { data: 'slug' },
                { data: 'category.title' },
                { data: 'featured' ,
                  render: function ( data, type, row ) {
                    if(row.featured == 1){
                      return '<span class="fas fa-star text-warning s-c"></span>';
                    } else {
                      return '<span class="far fa-star text-warning s-c"></span>';
                    }
                  }
                },
                { data: 'status' ,
                  render: function ( data, type, row ) {
                    if(row.status == 1){
                      return '<span class="badge badge-success">Enable</span>';
                    } else {
                      return '<span class="badge badge-danger">Disabled</span>';
                    }
                  }
                },
                { data: 'id',
                  render: function(data, type, row) {
                    let e_url = ("{{ route('blogs.edit',['id'=>'-id-'])}}").replace('-id-', row.id);
                    let d_url = ("{{ route('blogs.delete',['id'=>'-id-'])}}").replace('-id-', row.id);
                    return `<a class="btn btn-info btn-sm text-white cursor-pointer" href="${e_url}"> <i class="fa fa-edit"></i> </a>
                    <a class="btn btn-danger btn-sm text-white cursor-pointer" href="${d_url}"> <i class="fa fa-trash"></i>`;
                  }
                },
                               
              ],
              "drawCallback": function( settings ) {
                //   $('#search-result-div').show();
                //   $('#search-overlay').hide();
                //   initShowDetailsMethod();
              } //end drawcallback
    });
}

</script>
@endpush