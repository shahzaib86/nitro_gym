@extends('layouts.admin.app')
@section('page_header') Add Blog Category @endsection
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                <form method="post" enctype="multipart/form-data">
                        @csrf
                        <!-- text input -->
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" id="title" name="title" class="form-control"
                                {{-- value="{{ $emailtemplate->type }}"  --}}
                                placeholder="title">
                            @error('title')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="col-form-label" for="example-textarea">Description</label>
                            {{-- <div class="col-sm-10"> --}}
                                <textarea class="form-control"  id="example-textarea" name="description" ></textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="status" id="status1" data-toggle="toggle" data-on="Enabled" data-off="Disabled">
                            <label for="status1">Status</label>
                            @error('status')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="featured" id="featured" data-toggle="toggle" data-on="featured" data-off="Disabled">
                            <label for="featured">Featured</label>
                            @error('featured')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-info pull-right">Save</button>

                </div>
                </form>
            </div>
        </div>
    </div>

@endsection