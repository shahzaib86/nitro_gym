@extends('layouts.admin.app',['title'=>'Roles - '])

@push('css')

@endpush
@section('page_header') Roles @endsection
@section('content')

<div class="row">
    <div class="col-md-6">
        
      <div class="card-box">
        <h4 class="mt-0 header-title">Roles</h4>
        <p class="text-muted font-14 mb-3">  </p>

        <div class="table-responsive">
            <table class="table mb-0">
                <thead>
                <tr>
                    <th>ID#</th>
                    <th>Title</th>
                    <th>Assigned to</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach ($roles as $item)
    
                  <tr>
                    <th scope="row">{{$item->id}}</th>
                    <td>{{$item->name}}</td>
                    <td>{{$item->users->count()}} user(s)</td>
                    <td>
                      <a href="{{route('roles.edit',['role'=>$item->id])}}">
                        <button type="button" class="btn btn-primary btn-sm">
                        <i class="fas fa-edit"></i></button>
                      </a>
                    </td>

                  </tr>
                      
                @endforeach

                </tbody>
            </table>
        </div>
      </div>

    </div>
</div>


@endsection


@push('scripts')



@endpush