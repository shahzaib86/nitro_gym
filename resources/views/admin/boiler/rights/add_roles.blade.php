@extends('layouts.admin.app',['title'=>'Role - '])

@push('css')

@endpush
@section('page_header') Role @endsection
@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card-box">

            <h4 class="mt-0 mb-3 header-title">
                @if ($role) Update Role 
                @else Add New Role @endif
            </h4>

            <form role="form" method="POST" 
            @if ($role)
                action="{{route('role.update',['role'=>$role])}}"
            @else
                action="{{route('role.create')}}"
            @endif>
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name" @if($role) value="{{$role->name}}" @endif aria-describedby="titleHelp" placeholder="Enter title">
                    @error('name')
                        <small id="nameHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label> Select Permissions</label>
                    <p class="solid">
                        <div class="form-group2" style="overflow-y: scroll; height:300px;">
                    @if($role)
                        @foreach ($role->permissions as $key=>$item)
                            <input type="checkbox" id="permission{{$key}}" name="permissions[]" value="{{$item->id}}" checked>
                            <label for="permission{{$key}}">{{$item->name}}</label><br>
                        @endforeach
                    @endif
                        @foreach($permissions as $key=>$permission)
                            <input type="checkbox" id="permission{{$key}}" name="permissions[]" value="{{$permission->id}}" >
                            <label for="permission{{$key}}">{{$permission->name}}</label><br>
                        @endforeach
                    </div>
                </div>
                
                <input type="submit" class="btn btn-primary" value="Submit" >
            </form>
        </div>
    </div>
</div>


@endsection


@push('scripts')



@endpush