@extends('layouts.admin.app',['title'=>'User - ' ])

@push('css')


@endpush
@section('page_header') User @endsection
@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card-box">

            <h4 class="mt-0 mb-3 header-title">
                @if ($user) Update User 
                @else Add New User @endif
            </h4>

            <form role="form" method="POST" 
            @if ($user)
                action="{{route('admin.update',['id'=>$user->id])}}"
            @else
                action="{{route('admin.store')}}"
            @endif>
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name" @if($user) value="{{$user->name}}" @else value="{{old('name')}}" @endif aria-describedby="titleHelp" placeholder="Enter title">
                    @error('name')
                        <small id="nameHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" @if($user) value="{{$user->email}}" @else value="{{old('email')}}" @endif aria-describedby="emailHelp" placeholder="Enter email">
                    @error('email')
                        <small id="emailHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" aria-describedby="passwordHelp" placeholder="Enter password">
                    @error('password')
                        <small id="passwordHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="phone">Contact</label>
                    <input type="text" class="form-control" name="phone" id="phone" @if($user) value="{{$user->phone}}" @else value="{{old('phone')}}" @endif aria-describedby="phoneHelp" placeholder="Enter phone (ex: 03001234567)">
                    @error('phone')
                        <small id="phoneHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="role">Role</label>
                    <select class="form-control" id="role" name="role">
                        @if($user)
                            <option value="{{ $user->role->id }}" hidden selected>{{ $user->role->name }}</option>
                        @else
                            <option value=""> Select role </option>
                        @endif
                        @foreach (App\Boiler\Role::get()->all() as $item)
                            <option {{ $item->id == 1 ? 'hidden' : '' }} {{ old('role') != 1 && $item->id == 2 ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                    @error('role')
                        <small id="roleHelp" class="t-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="checkbox checkbox-primary pl-3">
                        <input type="checkbox" class="control-input" id="status" name="status" @if( $user && $user->status == 1 ) checked @endif >
                        <label for="status">
                            Enable
                        </label>
                    </div>
                </div>
                
                <input type="submit" class="btn btn-primary" value="Submit" >
            </form>
        </div>
    </div>
</div>


@endsection


@push('scripts')



@endpush