@extends('layouts.admin.app')

@push('css')
<!-- third party css -->
<link href="{{asset('admin/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endpush
@section('page_header') Admins @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <a href="{{ route('admin.create') }}" class="my-2 btn btn-success">
                Add New
                <i class="fa fa-plus"></i>
            </a>
            <div class="card-box table-responsive">

                {{-- <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                       <label for="users">Admins</label>
                                    <select class="select2" id="users" name="users[]" multiple="multiple">
                                            <option value=""> Select to search </option>
                                            @foreach(App\user::all() as $data)
                                              <option value="{{$data->id}}">{{$data->name}}</option>
                                            @endforeach
                                    </select>
                    </div>
                </div>
                
            </div> --}}

                <table id="adminlistTable"  class="table table-bordered dt-responsive nowrap" style="width:100%;">
                    <thead>
                        <tr>
                            <th width="10px">#</th>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Mail</th>
                            <th>Contact</th>
                            <th>Status</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        {{-- @php $i = 0 @endphp
                        @foreach ($result as $result)
                            @php $i++ @endphp
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $result->name }} </td>
                                <td>
                                    <a href="{{ route('food_item-edit', [$result->id]) }}" class="btn btn-info ">
                                        Edit </a>
                                    <a href="{{ route('food_item-delete', [$result->id]) }}"
                                        class="delete_user btn btn-danger " data-user_id=""> Delete</a>
                                </td>
                            </tr>
                        @endforeach --}}
                    </tbody>
                </table>
            </div>

        </div>
    </div>




@endsection


@push('scripts')
<!-- third party js -->
<script src="{{asset('admin/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('admin/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<!-- third party js ends -->
<script>


search_start();


// $('#users').change(function(){
//     search_start();
// })

function search_start(){
      if ($.fn.DataTable.isDataTable("#adminlistTable")) $("#adminlistTable").DataTable().destroy();
      //Start getting filters

    //   let users = $('#users').val();
      
      $('#adminlistTable').DataTable({
        "scrollX": true,
       processing: true,
       serverSide: true,
       ajax: {
            url: "{{route('admin.fetch')}}",
            type: "POST",
            data: {
              "_token": "{{ csrf_token() }}",
            }
        },
       createdRow: function( row, data, dataIndex ) {
            // $(row).attr('class', 'data-row click_detail');
            // $(row).attr('data-id', data.id);
            // console.log(row,data);
        },
       columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                { data: 'name' },
                { data: 'role.name' },
                { data: 'email' },
                { data: 'phone' },
                { data: 'status' ,
                  render: function ( data, type, row ) {
                    if(row.status == 1){
                      return '<span class="badge badge-success">Enable</span>';
                    } else {
                      return '<span class="badge badge-danger">Disabled</span>';
                    }
                  }
                },
                { data: 'id',
                  render: function(data, type, row) {
                    let e_url = ("{{ route('admin.edit',['id'=>'-id-'])}}").replace('-id-', row.id);
                    let d_url = ("{{ route('admin.delete',['id'=>'-id-'])}}").replace('-id-', row.id);
                    return `<a class="btn btn-info btn-sm text-white cursor-pointer" href="${e_url}"> <i class="fa fa-edit"></i> </a>
                    <a class="btn btn-danger btn-sm text-white cursor-pointer" href="${d_url}"> <i class="fa fa-trash"></i>`;
                  }
                },
                               
              ],
              "drawCallback": function( settings ) {
                //   $('#search-result-div').show();
                //   $('#search-overlay').hide();
                //   initShowDetailsMethod();
              } //end drawcallback
    });
}


            // $(document).ready(function() {
            //     $('.select2').select2();
            // });

</script>
@endpush
