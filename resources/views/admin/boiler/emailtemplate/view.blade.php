@extends('layouts.admin.app')
@section('page_header') Email Templates @endsection
@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($EmailTemplates as $emailtemplate)
                        <tr>
                            <td>{{ Str::limit($emailtemplate->type, 25) }}</td>
                            <td>
                                <a class="btn btn-info btn-sm text-white cursor-pointer" href="{{ route('email.template.edit', ['id'=>$emailtemplate->id])}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- container-fluid -->

</div>
<!-- content -->
@endsection
