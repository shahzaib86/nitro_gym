@extends('layouts.admin.app')
@section('page_header') Edit Email Template @endsection
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                <form method="post" enctype="multipart/form-data">
                        @csrf
                        <!-- text input -->
                        <div class="form-group">
                            <label for="type">Type</label>
                            <input type="text" id="type" name="type" class="form-control"
                                value="{{ $emailtemplate->type }}" placeholder="type">
                            @error('type')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="template">Template</label>
                            <textarea type="text" id="template" name="template" class="form-control summernote">{{ $emailtemplate->template }}</textarea>
                            @error('template')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-info pull-right">Update</button>

                </div>
                </form>
            </div>
        </div>
    </div>

@endsection