<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title> {{ config('app.name', 'Admin Login') }} </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('admin/images/favicon.png') }}">

        <!-- App css -->
        <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/css/app.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/css/main.css')}}" rel="stylesheet" type="text/css" />

    </head>


    <body class="authentication-bgx" style="background: #282e38;">
        <img src="{{asset('admin/images/n-dark.png')}}" alt="" style="position: absolute;
            height: 100%;
            right: 0;
            bottom: 0px;
            opacity: 0.1;">
        <div class="home-btn d-none d-sm-block">
            {{-- <a href=""><i class="fas fa-home h2 text-dark"></i></a> --}}
        </div>

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="text-center">
                            
                        </div>
                        <div class="card">

                            <div class="card-body p-4">
                                
                                <div class="text-center mb-4">
                                    {{-- <h4 class="text-uppercase mt-0">Sign In</h4> --}}
                                    <img src="{{asset('admin/images/nitrogen-light.png')}}" alt="" height="34">
                                </div>

                                @if( Session::has('message') )
                                    <div class="alert alert-{{Session::get('type')}}">
                                        {{Session::get('message')}}
                                    </div>
                                @endif

                                <form action="{{route('admin.login.post')}}" method="POST" id="loginForm">
                                    @csrf
                                    <div class="form-group mb-3">
                                        <label for="email">{{ __('E-Mail') }}</label>
                                        <input class="form-control" type="email" id="email" name="email" required value="{{ old('email') }}" placeholder="Enter your email">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" name="password" required id="password" placeholder="Enter your password">
                                    </div>

                                    {{-- <div class="form-group mb-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="checkbox-signin" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="checkbox-signin">{{ __('Remember Me') }}</label>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="error_msg"> {{ $message }} </span>
                                                </span>
                                            @enderror
                                        </div>
                                    </div> --}}

                                    <div class="form-group mb-0 text-center">
                                        <input value="Log In" class="btn btn-primary btn-block" type="submit">
                                    </div>

                                </form>

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                {{-- <p> <a href="#" class="text-muted ml-1"><i class="fa fa-lock mr-1"></i>Forgot your password?</a></p> --}}
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->
    

        <!-- Vendor js -->
        <script src="{{asset('admin/js/vendor.min.js')}}"></script>

        <!-- App js -->
        <script src="{{asset('admin/js/app.min.js')}}"></script>

        <!-- Main js -->
        <script src="{{asset('admin/js/main.js')}}"></script>
            
    </body>
</html>
