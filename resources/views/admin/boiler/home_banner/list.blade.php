@extends('layouts.admin.app')

@push('css')
<style>
  .on_dragging {
    background: yellowgreen;
  }
  .dnd_table {
    border: 1px solid #dee2e6;
  }
  td{
    vertical-align: middle !important;
  }
</style>
@endpush
@section('page_header') Home Banners @endsection
@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <a href="{{route('home.banner.add')}}" class="my-2 btn btn-success">
              <i class="fa fa-plus"></i>
                Add New
            </a>
            <div class="card-box table-responsive">
              <form action="{{route('home.banner.update.sequence')}}" method="POST">    
                @csrf
                <table id="bannersTable" class="table table-bordered dt-responsive nowrap" style="width:100%;">
                    <thead>
                        <tr>
                          <th >#</th>
                          <th width="20%"> Preview </th>
                          <th width="30%"> Title </th>
                          <th width="30%"> link </th>
                          <th width="5%"> Status </th>
                          <th width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($banners as $key=> $item)
                        <tr>
                            <td> <input type="hidden" name="sequence[]" value="{{ $item->id }}"> <i class="fa fa-arrows-alt-v"></i> &nbsp;&nbsp; {{$key+1}}</td>
                            <td width="20%"> <img src="{{($item->banner_path)}}" style="width:100%;" /> </td>
                            <td width="30%">{{ $item->title }}</td>
                            <td width="30%">{{ $item->link }}</td>
                            <td> {!! $item->status==1?'<span class="badge badge-success">Enable</span>':'<span class="badge badge-danger">Disabled</span>' !!} </td>
                            <td>
                                <a class="btn btn-info btn-sm text-white cursor-pointer" href="{{route('home.banner.edit',['id'=>$item->id])}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </form>
            </div>
        </div>
    </div>

    <!-- container-fluid -->

</div>
<!-- content -->
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/TableDnD/0.9.1/jquery.tablednd.js" integrity="sha256-d3rtug+Hg1GZPB7Y/yTcRixO/wlI78+2m08tosoRn7A=" crossorigin="anonymous"></script>
<script>
$(document).ready(function() {
    // Initialise the table
    $("#bannersTable").tableDnD({
        onDragClass:'on_dragging'
    });
});
</script>
@endpush