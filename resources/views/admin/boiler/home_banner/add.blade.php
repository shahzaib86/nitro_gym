@extends('layouts.admin.app')

@section('page_header') @if($banner) Edit @else Add @endif Banner @endsection
@push('css')
<!-- dropify -->
<link href="{{asset('admin/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="card-box">
                <form method="POST" @if($banner) action="{{route('home.banner.update',['id'=>$banner->id])}}" @else action="{{route('home.banner.save')}}" @endif enctype="multipart/form-data" id="banner_form">
                    @csrf
                    <!-- text input -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Title </label>
                                <input type="text" id="title" name="title"  @if($banner) value="{{$banner->title}}" @else value="{{ old('title')}}" @endif class="form-control"
                                    {{-- value="{{ $emailtemplate->type }}" --}} placeholder="Title">
                                @error('title')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Link</label>
                                <input type="text" id="link" name="link" @if($banner) value="{{$banner->link}}" @else value="{{ old('link')}}" @endif class="form-control" placeholder="Link">
                                @error('link')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Banner</label>
                                <label class=" col-form-label" for="example-fileinput"></label>
                                <input type="file" class="form-control dropify" id="banner_image" name="banner_image" @if($banner) data-default-file="{!! $banner->banner_path !!}" @endif>
                                @error('banner_image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div> 
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="checkbox" name="status" id="status1" data-toggle="toggle" data-on="Enabled"
                                    data-off="Disabled" @if($banner && $banner->status == 1) checked @endif>
                                <label for="status1">Enable</label>
                                @error('status')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>
                       
                    </div>
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                </form>
                <img id="Image" src="" style="width: 50%; margin-top:20px;" /> 
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<!-- dropify js -->
<script src="{{asset('admin/libs/dropify/dropify.min.js')}}"></script>
<script>
$(document).ready(function() {
    $(".dropify").dropify({messages:{default:"Drag and drop a banner here or click",replace:"Drag and drop or click to replace",remove:"Remove",error:"Ooops, something wrong appended."},error:{fileSize:"The file size is too big (1M max)."}});
    
});
</script>
@endpush
