<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Expense extends Model
{
    //
    protected $table = "expense";
    protected $guarded = ['id'];
    protected $appends = ['m_date'];

    public static $createRules = [
        'date' =>['required'],
        'amount' =>['required','numeric' ],
        'note' =>['required','max:500' ],
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function getMDateAttribute(){
        return Carbon::parse($this->date)->isoFormat('DD MMM, YYYY');
    }

}
