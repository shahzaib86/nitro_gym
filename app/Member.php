<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Fees;

class Member extends Model
{
    //
    protected $table = 'members';

    protected $guarded = ['id'];
    protected $appends = ['m_join_date'];

    public static $createRules = [
        'member_no' => ['required', 'string', 'max:20' , 'unique:members'],
        'name' => ['required', 'string', 'max:50'],
        'father_name' => ['required', 'string', 'max:50'],
        'email' => ['nullable', 'string', 'max:50','regex:/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/', 'unique:users' ],
        'mobile' =>['required','string','min:11','max:15' ],
        'phone' =>['nullable','string','min:11','max:15' ],

        'gender' =>['required','string' ],
        'age' =>['nullable','string','max:15' ],
        'height' =>['nullable','string','max:15' ],
        'weight' =>['nullable','string','max:15' ],

        'occupation' =>['nullable','string'],
        'timing' =>['required','string', 'max:100' ],

        'address' =>['required','string', 'max:500' ],
        'cnic' =>['nullable','string','max:15' ],

        'branch' =>['nullable','string', 'max:100' ],
        'medical_problem' =>['nullable','string', 'max:500' ],

        'package' =>['required','numeric', 'min:1' ],
    ];

    static public function updateRule($id){
        return [
            'member_no' => ['required', 'string', 'max:20' , 'unique:members,member_no,'.$id],
            'name' => ['required', 'string', 'max:50'],
            'father_name' => ['required', 'string', 'max:50'],
            'email' => ['nullable', 'string', 'max:50','regex:/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/', 'unique:users' ],
            'mobile' =>['required','string','min:11','max:15' ],
            'phone' =>['nullable','string','min:11','max:15' ],

            'gender' =>['required','string' ],
            'age' =>['nullable','string','max:15' ],
            'height' =>['nullable','string','max:15' ],
            'weight' =>['nullable','string','max:15' ],

            'occupation' =>['nullable','string'],
            'timing' =>['required','string', 'max:100' ],

            'address' =>['required','string', 'max:500' ],
            'cnic' =>['nullable','string','max:15' ],

            'branch' =>['nullable','string', 'max:100' ],
            'medical_problem' =>['nullable','string', 'max:500' ],

            'package' =>['required','numeric' ],
        ];
    }

    public function fees(){
        return $this->hasMany('App\Fees','member_id');
    }

    static public function getActive(){
        return Member::where('status',1)->orderby('member_no','ASC')->get();
    }

    public function getLastSubmittedFees(){
        $fee = Fees::where('member_id',$this->id)->orderby('date','DESC')->first();
        return $fee?'(Last Fee: '.$fee->m_date.')':'-';
    }

    public function getMJoinDateAttribute(){
        return Carbon::parse($this->join_date)->isoFormat('DD MMM, YYYY');
    }

}
