<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','status','google_id','facebook_id','role_id','avatar','code','is_deleted','franchise_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static $adminRules = [
        'name' => ['required', 'string', 'max:30'],
        'email' => ['required', 'string', 'max:50','regex:/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/', 'unique:users' ],
        'password' => ['required', 'string', 'min:8','max:20', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[@!$#%]).*$/'],
        'phone' =>['nullable','string','min:11','max:15', 'unique:users' ],
        'role' =>['required'],
    ];

    public static function getAdminRulesUpdate($id){
        return [
            'name' => ['required', 'string', 'max:30'],
            'email' => ['required', 'string', 'max:50','regex:/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/', 'unique:users,email,'.$id],
            'password' => ['nullable','string', 'min:8','max:20', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[@!$#%]).*$/'],
            'phone' =>['nullable','string','min:11','max:15', 'unique:users,phone,'.$id],
            'role' =>['required'],
        ];
    }
    

    public function isAdmin()
    {
        return ($this->role=='superadmin' || $this->role=='admin');
    }
    
    public function isSuperAdmin()
    {
        return $this->role=='superadmin';
    }
    
    public function isCustomer(){
        if($this->role=='customer'||$this->role=='Customer' || $this->role == 'guest' || $this->role== 'Guest')
            return true;
        else{
            return false;
        }
    }

    public function role(){
        return $this->belongsTo('App\Boiler\Role');
    }

    public function roles(){
        return $this->belongsToMany(Boiler\Role::class,'b_users_roles');
    }

}
