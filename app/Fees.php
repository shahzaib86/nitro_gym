<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Fees extends Model
{
    //
    protected $table = "fees";
    protected $guarded = ['id'];
    protected $appends = ['m_date'];

    public static $createRules = [
        'member' => ['required'],
        'date' =>['required'],
        'amount' =>['required','numeric','min:1'],
    ];

    public function member(){
        return $this->belongsTo('App\Member','member_id');
    }

    public function getMDateAttribute(){
        return Carbon::parse($this->date)->isoFormat('DD MMM, YYYY');
    }

}
