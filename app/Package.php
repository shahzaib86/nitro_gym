<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    protected $table = 'package';

    protected $guarded = ['id'];

    public static $createRules = [
        'name' => ['required', 'string', 'max:50'],
        'type' => ['required', 'string', 'max:50'],
        'amount' =>['required','numeric' ],
    ];
}
