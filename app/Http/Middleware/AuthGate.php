<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Gate;
use Closure;
use App\Boiler\Role;
use App\User;

class AuthGate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=auth()->user();
        if(!app()->runningInConsole() && $user){
            $roles=Role::with('permissions')->get();
            
            foreach ($roles as $role ) {
                foreach ($role->permissions as $permissions) {
                    $permissionArray[$permissions->name][]=$role->id;
                }
            }
            foreach ($permissionArray as $title => $roles) {
                Gate::define($title,function(\App\User $user) use($roles){
                    return count(array_intersect($user->roles->pluck('id')->toArray(),$roles)) > 0;
                });
            }
        }
        return $next($request);
    }
}
