<?php

namespace App\Http\Common;

use Illuminate\Http\Request;
use App\Http\Boiler\BHelper;
use Session;

class Helper extends BHelper {
    
    public static function toast($type,$message,$title='') {
        Session::flash('title',$title);
        Session::flash('type',$type);
        Session::flash('message',$message);
    }

    public static function sweetAlert($type,$message,$title='') {
        Session::flash('title',$title);
        Session::flash('type',$type);
        Session::flash('message',$message);
    }
}