<?php

namespace Boiler;

abstract class BEnum {

    const ERR_MSG_500 = 'Internal Server Error ';
    const ERR_MSG_422 = 'Fields Missing ';

    const LOG_LOGIN = 1;
    
    const LOG_CANDELA_ORDER = 2;
    const LOG_ORDER_STATUS = 3;
    const LOG_ORDER_ADD_PRODUCT = 4;
    const LOG_ORDER_DELETE_PRODUCT = 5;
    const LOG_ORDER_UPLOAD_PRES = 13;
    const LOG_ORDER_DELETE_PRES = 14;

    const LOG_HOME_SECTION_SEQUENCE = 6;

    const LOG_ADD_PRODUCT_SECTION = 7;
    const LOG_ADD_BRAND_SECTION = 8;
    const LOG_ADD_CATEGORY_SECTION = 9;

    const LOG_UPDATE_PRODUCT_SECTION = 10;
    const LOG_UPDATE_BRAND_SECTION = 11;
    const LOG_UPDATE_CATEGORY_SECTION = 12;
    
}
