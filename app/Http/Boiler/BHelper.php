<?php

namespace App\Http\Boiler;

use Illuminate\Http\Request;
use App\User;
use Session;
use Auth;

class BHelper {

    public static function toast($type,$message,$title='') {
        Session::flash('title',$title);
        Session::flash('type',$type);
        Session::flash('message',$message);
    }

    public static function sweetAlert($type,$message,$title='') {
        Session::flash('title',$title);
        Session::flash('type',$type);
        Session::flash('message',$message);
    }

}