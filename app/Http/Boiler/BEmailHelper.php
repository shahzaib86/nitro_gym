<?php

namespace Boiler;

use Illuminate\Http\Request;
use App\User;
use Session;
use Auth;
use PHPMailer\PHPMailer;
use Carbon\Carbon;
use Mail;

class BEmailHelper {

	private $host = "";
	private $username = "";
	private $password = "";
	private $from_address = "";
	private $from_name = "";
 
    public function __construct() 
    {
		$this->host = env('MAIL_GUN_HOST');
		$this->username = env('MAIL_GUN_USERNAME');
		$this->password = env('MAIL_GUN_PASSWORD');
		$this->from_address = env('MAIL_GUN_FROM_ADDRESS');
		$this->from_name = env('MAIL_GUN_FROM_NAME');
    }

    static public function sendEmail($name,$email,$subject,$template,$params=[],$pdf_params=[],$cc=false) {
        foreach( $params as $k=>$v ){
            $template = str_ireplace("{{".$k."}}", $v, $template);
        }
        $text             = $template;
        $mail             = new PHPMailer\PHPMailer(); // create a n
        $mail->SMTPDebug  = 1; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth   = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail

        $mail->Host       = env('MAIL_GUN_HOST');

        if( env('MAIL_GUN_CC_ADDRESS') ){
            $mail->addCC( env('MAIL_GUN_CC_ADDRESS') ,env('MAIL_GUN_CC_NAME') );
        }

        $mail->Port       = env('MAIL_GUN_PORT'); // 465 or 587
		$mail->Username = env('MAIL_GUN_USERNAME');
		$mail->Password = env('MAIL_GUN_PASSWORD');
        $mail->SetFrom(env('MAIL_GUN_FROM_ADDRESS'), env('MAIL_GUN_FROM_NAME'));

        $mail->Subject = $subject;
        $mail->Body    = $text;
        $mail->AddAddress($email, $name);

        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }
    }

}
