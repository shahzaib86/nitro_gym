<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Common\Helper;
use Illuminate\Support\Facades\Hash;
use Auth;
use DB;
use Illuminate\Support\Str;
use Yajra\Datatables\Datatables;


class AdminUserController extends Controller
{
    //
    public function index() {
        // $data['users'] = User::all();
        return view('admin.boiler.admins.list');
    }
    
    public function fetch(Request $request){

        $query = User::whereNotIn('role_id',[1])->where('is_deleted',0)->with(['role'])->get();

        return Datatables::of($query)->addIndexColumn()->make(true);

    }

    public function create(){
        $data['user'] = null;
        return view('admin.boiler.admins.add',$data);
    }

    public function store(Request $request) {

        $validate = $request->validate(User::$adminRules);
        try {
            $status = 0;
            if ($request->status) { $status = 1; }
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'phone'=> $request->phone,
                'role_id' => $request->role,
                'status' => $status,
                'avatar' => "profile_pictures/avatar.jpg",
            ]);
            Helper::toast('success','User Created Sucessfully.');
            DB::table('b_users_roles')->insert(['user_id'=>$user->id,'role_id'=>$request->role]);
            return redirect()->route('admin.show');
        } catch(\Exception $e) {
            Helper::toast('error','Something went wrong.');
            return back();
        }
                
    }

    public function edit($id){
        $data['user'] = User::findOrFail($id);
        return view('admin.boiler.admins.add',$data);
    }

    public function update(Request $request,$id) {
        
        $validate = $request->validate(User::getAdminRulesUpdate($id));

        try {
            $status = 0;
            if ($request->status) { $status = 1; }
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'phone'=> $request->phone,
                'role_id' => $request->role,
                'status' => $status
            ];
            if($request->password!=''&&$request->password!=null){
                $data['password'] = Hash::make($request->password);
            }
            $user = User::find($id)->update($data);
            Helper::toast('success','User Created Sucessfully.');
            DB::table('b_users_roles')->where('user_id', $id)->update(['role_id'=>$request->role]);
            return redirect()->route('admin.show');
        } catch(\Exception $e) {
            Helper::toast('error','Something went wrong.');
            return back();
        }
                
    }

    public function delete($id){
        User::find($id)->update(['is_deleted'=>1]);
        Helper::toast('success','Deleted');
            return back();
    }

}
