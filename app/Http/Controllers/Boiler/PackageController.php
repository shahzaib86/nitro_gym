<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Package;
use App\Http\Common\Helper;
use Auth;
use DB;
use Illuminate\Support\Str;
use Yajra\Datatables\Datatables;

class PackageController extends Controller
{
    //
    public function index() {
        // $data['users'] = User::all();
        return view('admin.package.list');
    }
    
    public function fetch(Request $request){

        $query = Package::where('id','<>',0);

        if( $request->status != '' ){
            $query = $query->where('status',$request->status);
        }

        // $query = $query;

        return Datatables::of($query)->addIndexColumn()->make(true);

    }

    public function create(){
        $data['user'] = null;
        return view('admin.package.add',$data);
    }

    public function insertUpdate($request, $id=''){
        if( $id != '' ) {
            $member = Package::where(['id'=>$id])->first();
        } else {
            $member = new Package;
        }

        if ($request->status) { $status = 1; }
        else { $status = 0; }

        $member->name = $request->name;
        $member->type = $request->type;
        $member->amount = $request->amount;
        $member->status = $status;

        $member_check = $member->save();

        if($member_check) {
            return true;
        } else {
            return false;
        }

    }

    public function store(Request $request) {
        $validate = $request->validate(Package::$createRules);
        $insert_check = $this->insertUpdate($request);
        if($insert_check) {
            Helper::toast("success","Package has been sucessfuly created.","Success");
        } else {
            Helper::toast("error","Failed to add package.","Error");
        }
        return back();
    }

    public function edit($id){
        $data['user'] = Package::findOrFail($id);
        return view('admin.package.add',$data);
    }

    public function update(Request $request,$id) {
        $validate = $request->validate(Package::$createRules);
        $insert_check = $this->insertUpdate($request,$id);
        if($insert_check) {
            Helper::toast("success","Package has been updated successfully.","Success");
        } else {
            Helper::toast("error","Failed to update package.","Error");
        }
        return back();
    }

    public function delete($id){
        Package::find($id)->delete();
        Helper::toast('success','Deleted');
        return back();
    }

}
