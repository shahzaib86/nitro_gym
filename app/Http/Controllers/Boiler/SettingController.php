<?php

namespace App\Http\Controllers\boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Common\Helper;
use App\Boiler\Setting;

class SettingController extends Controller
{
    //

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['settings'] = Setting::all(['id','key','value','title','description','json_params']);
        // foreach( $data['settings'] as $k=>$v ){
        //     $data['settings'][$k]['params'] = $data['settings'][$k]->json_params;
        // }

        // return $data;
        // admin.boiler.pages.edit
		return view('admin.boiler.settings.list',$data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        unset($request['_token']);
		foreach( $request->input() as $k=>$v ){
			Setting::where('key','like', $k)
	          ->update(['value' => $v]);
        }
        Helper::toast('success','Settings updated');
        // Helper::sweetAlert('success','Settings updated','');
		return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(setting $setting)
    {
        //
    }

}
