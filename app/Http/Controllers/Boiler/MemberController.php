<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Member;
use App\Fees;
use App\Http\Common\Helper;
use Auth;
use DB;
use Illuminate\Support\Str;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class MemberController extends Controller
{
    //
    public function index() {
        // $data['users'] = User::all();
        return view('admin.members.list');
    }
    
    public function fetch(Request $request){

        $query = Member::where('id','<>',0);

        if( $request->status != '' ){
            $query = $query->where('status',$request->status);
        }

        // $query = $query;

        return Datatables::of($query)->addIndexColumn()->make(true);

    }

    public function create(){
        $data['user'] = null;
        return view('admin.members.add',$data);
    }

    public function insertUpdate($request, $id=''){
        $new_member = false;
        if( $id != '' ) {
            $member = Member::where(['id'=>$id])->first();
        } else {
            $member = new Member;
            $count = Member::count()+1;
            $join_date = date('Y-m-d');
            $y_m = date('Y');
            // $member->join_date = $join_date;
            // $member->member_no = 'N'.$y_m.'-'.$count;
            // $member->join_date = $request->join_date;
            // $member->member_no = $request->member_no;
            $new_member = true;
        }

        if ($request->status) { $status = 1; }
        else { $status = 0; }

        $format_join_date = Carbon::createFromFormat('d/m/Y', $request->join_date)->isoFormat('YYYY-MM-DD');

        $member->member_no = $request->member_no;
        $member->join_date = $format_join_date;
        $member->name = $request->name;
        $member->father_name = $request->father_name;
        $member->email = $request->email;
        $member->mobile = $request->mobile;
        $member->phone = $request->phone;
        $member->gender = $request->gender;
        $member->age = $request->age;
        $member->height = $request->height;
        $member->weight = $request->weight;
        $member->occupation = $request->occupation;
        $member->timing = $request->timing;
        $member->address = $request->address;
        $member->cnic = $request->cnic;
        $member->branch = $request->branch;
        $member->medical_problem = $request->medical_problem;
        $member->package = $request->package;
        $member->package_id = $request->package_id;
        $member->status = $status;

        $member_check = $member->save();

        if($member_check) {
            if( $new_member ){
                $fees = Fees::create([
                    'member_id' => $member->id,
                    'date' => $member->join_date,
                    'amount' => $member->package,
                    'package_id' => $member->package_id
                ]);
                $data['fees'] = $fees;
                return $data;
                // return view('admin.fees.fee_slip',$data);
            }
            return true;
        } else {
            return false;
        }

    }

    public function store(Request $request) {
        $validate = $request->validate(Member::$createRules);
        $insert_check = $this->insertUpdate($request);
        if($insert_check) {
            if( isset($insert_check['fees']) ){
                return view('admin.fees.fee_slip',$insert_check);
            }
            Helper::toast("success","Member has been sucessfuly created.","Success");
        } else {
            Helper::toast("error","Failed to add member.","Error");
        }
        return back();
    }

    public function edit($id){
        $data['user'] = Member::findOrFail($id);
        return view('admin.members.add',$data);
    }

    public function update(Request $request,$id) {
        $validate = $request->validate(Member::updateRule($id));
        $insert_check = $this->insertUpdate($request,$id);
        if($insert_check) {
            Helper::toast("success","Member has been updated successfully.","Success");
        } else {
            Helper::toast("error","Failed to update member.","Error");
        }
        return back();
    }

    public function delete($id){
        try{
            $count = Fees::where('member_id',$id)->count();
            if( $count == 1 ){
                Fees::where('member_id',$id)->delete();
                Member::find($id)->delete();
                Helper::toast('success','Deleted');
            } else {
                Helper::toast('error','Member cannot be deleted, can be mark disable only.');
            }
            return back();
        } catch(\Exception $e){
            Helper::toast('error','Something went wrong');
            return back();
        }
        
    }

}
