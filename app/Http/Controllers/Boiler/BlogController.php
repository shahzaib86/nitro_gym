<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Boiler\Blog;
use App\Boiler\BlogCategory;
use Illuminate\Support\Str;
use App\Http\Common\Helper;
use Auth;
use Yajra\Datatables\Datatables;

class BlogController extends Controller
{

    public function view() {
         return view('admin.boiler.blogs.blogs');
    }

    public function fetch(Request $request) {

        $query = Blog::with(['category'])->select(['b_blogs.id as sp','b_blogs.*']);

        if( $request->category != '' ) {
            $query = $query->where('blog_category_id',$request->category);
        }

        if( $request->status != '' ) {
            $query = $query->where('status',$request->status);
        }

        return Datatables::of($query)->make(true);
    }

    public function add(Request $request) {
        if($request->isMethod('post')){
            $request->validate([
                'title'=>'required',
                'description'=>'required',
            ]);

            $user = Auth::user();
            $data =[
                'title' => $request->title,
                'slug'=> str::slug($request->title,'-'),
                'description' => $request->description,
                'blog_category_id'=> $request->category,
                'auther_id'=> $user->id,
            ];

            if($request->status) {
                $data['status'] = 1;
            } else {
                $data['status'] = 0;
            }

            if($request->featured) {
                $data['featured'] = 1;
            } else {
                $data['featured'] = 0;
            }

            if ($request->file('file')) {
                $imagePath = $request->file('file');
                $imageName = $imagePath->getClientOriginalName();
                $path = $request->file('file')->storeAs('blogs', $imageName, 'public');
                $data['image'] = $path;
            }

            Blog::create($data);

            Helper::toast('success','Added');
            return redirect(route('blogs.show'));

        } else {
            $categories = BlogCategory::all();
            return view('admin.boiler.blogs.blogs_add',Compact('categories'));
        }
    }

    public function edit(Request $request,$id) {

        if($request->isMethod('post')){

            $request->validate([
                'title'=>'required',
                'description'=>'required',
                ]);

                $user = Auth::user();

            $data =[
                'title' => $request->title,
                'slug'=> str::slug($request->title,'-'),
                'description' => $request->description,
                'blog_category_id'=> $request->category,
                'auther_id'=> $user->id,
            ];

            if($request->status){
                $data['status'] = 1;
            } else {
                $data['status'] = 0;
            }

            if($request->featured){
                $data['featured'] = 1;
            } else {
                $data['featured'] = 0;
            }

            if ($request->file('file')) {
                $imagePath = $request->file('file');
                $imageName = $imagePath->getClientOriginalName();
                $path = $request->file('file')->storeAs('blogs', $imageName, 'public');
                $data['image'] = $path;
            }

            Blog::find($id)->update($data);
            Helper::toast('success','Updated');
            return redirect(route('blogs.show'));
        
        } else {
            $data['categories'] = BlogCategory::all();
            $data['blog']= Blog::find($id);
            return view('admin.boiler.blogs.blogs_edit',$data);
        }
    }

    public function delete($id){
        Blog::where('id',$id)->delete();
        Helper::toast('success','Deleted');
        return back();
    }

}
