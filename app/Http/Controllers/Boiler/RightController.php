<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Boiler\Permission;
use App\Boiler\Role;
use DB;
use App\Http\Common\Helper;
use App\User;

class RightController extends Controller
{
    
    public function show_roles()
    {
        $roles=Role::get();
        return view('admin.boiler.rights.roles_list',compact('roles'));
    }
    public function add_role(){
        $role=null;
        $permissions=Permission::orderBy('name','ASC')->get()->all();
        return view('admin.boiler.rights.add_roles',compact('permissions'))->with('role',$role);
    }
    public function create_role(Request $request)
    {
        $data=request()->validate([
            'name'=>'required|max:100',
            'permissions[]'=>'',
        ]);
        try {
            $role=Role::create(['name'=>$data['name']]);
            foreach ($request->input('permissions') as $perm_id) {
                DB::table('b_roles_permissions')->insert(['role_id'=>$role->id,'permission_id'=>$perm_id]);
            }
            Helper::toast('success','Role created.');
            return back();
            // return redirect()->route('roles.show');
        } catch (\Throwable $th) {
            Helper::toast('success','Role creation failed.');
            return back();
            // return redirect()->route('roles.show');
        }

    }

    public function update_role(Request $request,Role $role)
    {
        $data=request()->validate(['name'=>'required|max:100','permissions'=>'required']);
        $role->update(['name'=>$data['name']]);
        DB::table('b_roles_permissions')->where('role_id',$role->id)->delete();
        foreach ($request->input('permissions') as $perm_id) {
            DB::table('b_roles_permissions')->insert(['role_id'=>$role->id,'permission_id'=>$perm_id]);
        }
        Helper::toast('success','Role updated.');
        return back();
        return redirect()->route('roles.show');
        
    }
    public function edit_role(Role $role)
    {
        $selected_permission=$role->permissions;
        $nonSelected_permission=[];
        $permissions=Permission::get()->all();
        foreach ($permissions as $key=> $perm) {
            foreach ($selected_permission as $sel_perm) {
                if($sel_perm->id==$perm->id){
                    unset($permissions[$key]);
                }
            }
        }
        return view('admin.boiler.rights.add_roles',compact('role'),compact('permissions'));
    }

}
