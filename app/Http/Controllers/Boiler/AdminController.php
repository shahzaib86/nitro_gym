<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Member;
use App\Fees;
use Carbon\Carbon;
use App\Http\Common\Helper;
use Illuminate\Support\Facades\Hash;
use Auth;

class AdminController extends Controller
{
    
    public function index() {
        if( Auth::check() ) {
            return redirect()->route('admin.home');
        }
        return view('admin.boiler.auth.login');
    }

    public function login(Request $request) {
        // return $request;
        $user = User::where(['email'=>$request->email])->whereHas('role', function ($query){  
        $query->whereNotIn('slug',['customer','guest']);})->with('role')->first();
        if($user) {
            if( Hash::check($request->password, $user->password) ) {
                Auth::login($user);
                
                return redirect()->route('admin.home');
            }
        }
        Helper::sweetAlert('danger','Incorrect Username/Password.');
        return back();
    }

    public function dashboard() {

        // if( $request->date_from && $request->date_to ) {
        if( true ) {

            // $date_from = Carbon::parse($request->date_from)->isoformat('YYYY-MM-DD');
            // $date_to = Carbon::parse($request->date_to)->isoformat('YYYY-MM-DD');

            $date_from = Carbon::parse(now())->subMonths(2)->isoformat('YYYY-MM-DD');
            $date_to = Carbon::parse(now())->addMonths(2)->isoformat('YYYY-MM-DD');

            $range = Carbon::parse($date_from)->range($date_to);
            $list = [];
            foreach($range as $date){
                $parsed = Carbon::parse($date)->isoFormat('MMM-YYYY');
                $pmon = Carbon::parse($date)->isoFormat('M');
                $pyear = Carbon::parse($date)->isoFormat('YYYY');
                $list[$parsed] = [
                    'month' => $pmon,
                    'year' => $pyear,
                    'data' => []
                ];
            }

            foreach( $list as $key=>$item ){
                $fee_list = Fees::whereRaw('MONTH(`date`) = '.$item['month'] )->whereRaw('YEAR(`date`) = '.$item['year'])->get();
                $arrange = [];
                foreach( $fee_list as $fee ){
                    $arrange[$fee->member->member_no] = $fee->amount;
                }
                $list[$key]['data'] = $arrange;
            }
            $members = Member::getActive();
            
            $tobe_sort = [];
            $member_wise_node = [];
            foreach($members as $item){
                $tobe_sort[] = $item->member_no;
                $member_wise_node[$item->member_no] = $item;
            }
            natsort($tobe_sort);
            $members_new = [];
            foreach($tobe_sort as $item){
                $members_new[] = $member_wise_node[$item];
            }
            
            // echo "<pre>";
            // print_r($members_new);
            // die();
            
            $members = $members_new;
            // return $list;
            $data['records'] = $list;
            $data['members'] = $members;
            $data['today'] = Carbon::parse(now())->isoFormat('YYYY-MM-DD');
            
        } else {
            $data['records'] = [];
            $data['members'] = [];
            $data['today'] = '';
        }

        // return $data;

        return view('admin.boiler.home',$data);
    }

}
