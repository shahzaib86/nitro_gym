<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Boiler\HomeBanner;
use Image;
use App\Http\Common\Helper;

class HomeController extends Controller
{
    
    public function updateSequence(Request $request) {
        $sections = $request->sequence;
        foreach( $sections as $sequence=>$id ) {
            $section = HomePage::find($id);
            $section->sequence = ($sequence+1);
            $section->save();
        }
        Helper::toast('success', 'Sequence updated.','Success');
        return back();
    }

    public function banners() {
        $data['banners'] = HomeBanner::orderBy('sequence','ASC')->get();
        return view('admin.boiler.home_banner.list',$data);
    }

    public function createBanner(){
        $data['banner'] = null;
        return view('admin.boiler.home_banner.add',$data);
    }
    
    public function saveBanner(Request $request){

        $request->validate([
            'title'=>['required'],
            'banner_image'=>['required','dimensions:width=1920,height=550'],
            'link'=>['nullable']
        ]);

        $imagePath = null;
        if (request('banner_image')) {
            $imagePath = \request('banner_image')->store('banner', 'public');
            $image = Image::make(public_path("storage/{$imagePath}"));
            $image->save();
            
            $sequence = (HomeBanner::all()->count()+1);
            if( $request->status == 'on' ) {  $status = 1; }
            else{ $status = 0; }

            if( $request->is_outside == 'on' ) {  $is_outside = 1; }
            else{ $is_outside = 0; }
            HomeBanner::create([
                'title' => $request->title,
                'banner_path' => $imagePath,
                'link' => $request->link,  
                'status' => $status,
                'is_outside' => $is_outside,
                'sequence' => $sequence,
            ]);
            Helper::sweetAlert('success','Banner added successfully.','Success');
        }
        return redirect()->route('home.banners');
    }

    public function editBanner($id){
        $data['banner'] = HomeBanner::find($id);
        // return $data;
        return view('admin.boiler.home_banner.add',$data);
    }

    public function updateBanner($id,Request $request){
        $imagePath = null;
        if (request('banner_image')) {
            $imagePath = \request('banner_image')->store('banner', 'public');
            $image = Image::make(public_path("storage/{$imagePath}"));
            $image->save();
            $data['banner_path'] = $imagePath;
        }
        $data['title'] = $request->title;
        $data['link'] = $request->link;
        
        if( $request->status == 'on' ) {  $data['status'] = 1; }
        else{ $data['status'] = 0; }

        if( $request->is_outside == 'on' ) {  $data['is_outside'] = 1; }
        else{ $data['is_outside'] = 0; }

        HomeBanner::find($id)->update($data);
        Helper::sweetAlert('success','Home Banner Update.','Success');
        return redirect()->route('home.banners');

    }

    public function updateSequenceBanners(Request $request) {
        $sections = $request->sequence;
        foreach( $sections as $sequence=>$id ) {
            $section = HomeBanner::find($id);
            $section->sequence = ($sequence+1);
            $section->save();
        }
        Alert::toast('success','Sections sequence updated.','Success');
        return back();
    }

}
