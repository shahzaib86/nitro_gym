<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Common\Helper;
use App\Fees;
use App\Member;
use DB;

class FeesController extends Controller
{
    //
    public function index(Request $request){

        if( $request->date_from && $request->date_to ) {
            $date_from = Carbon::parse($request->date_from)->isoformat('YYYY-MM-DD');
            // $date_from_month = Carbon::parse($request->date_from)->isoformat('M');
            // $date_from_year = Carbon::parse($request->date_from)->isoformat('YYYY');

            $date_to = Carbon::parse($request->date_to)->isoformat('YYYY-MM-DD');
            // $date_to_month = Carbon::parse($request->date_to)->isoformat('M');
            // $date_to_year = Carbon::parse($request->date_to)->isoformat('YYYY');

            $range = Carbon::parse($date_from)->range($date_to);
            $list = [];
            foreach($range as $date){
                $parsed = Carbon::parse($date)->isoFormat('MMM-YYYY');
                $pmon = Carbon::parse($date)->isoFormat('M');
                $pyear = Carbon::parse($date)->isoFormat('YYYY');
                $list[$parsed] = [
                    'month' => $pmon,
                    'year' => $pyear,
                    'data' => []
                ];
            }

            foreach( $list as $key=>$item ){
                $fee_list = Fees::whereRaw('MONTH(`date`) = '.$item['month'] )->whereRaw('YEAR(`date`) = '.$item['year'])->get();
                $arrange = [];
                foreach( $fee_list as $fee ){
                    $arrange[$fee->member->member_no] = $fee->amount;
                }
                $list[$key]['data'] = $arrange;
            }
            
            $members = Member::getActive();

            // return $list;
            $data['records'] = $list;
            $data['members'] = $members;
            $data['date_from'] = $date_from;
            $data['date_to'] = $date_to;
            
        } else {
            $data['records'] = [];
            $data['members'] = [];
            $data['date_from'] = '';
            $data['date_to'] = '';
        }

        // return view('admin.fees.fee_slip',$data);
        return view('admin.fees.month_report',$data);
    }

    public function revenue(Request $request){

        if( $request->date_from && $request->date_to ) {
            $date_from = Carbon::parse($request->date_from)->isoformat('YYYY-MM-DD');
            // $date_from_month = Carbon::parse($request->date_from)->isoformat('M');
            // $date_from_year = Carbon::parse($request->date_from)->isoformat('YYYY');

            $date_to = Carbon::parse($request->date_to)->isoformat('YYYY-MM-DD');
            // $date_to_month = Carbon::parse($request->date_to)->isoformat('M');
            // $date_to_year = Carbon::parse($request->date_to)->isoformat('YYYY');

            $range = Carbon::parse($date_from)->range($date_to);
            $list = [];
            
            // return $fees = Fees::whereRaw('DATE(`created_at`) BETWEEN '.$date_from.' AND '.$date_to )->get();

            $record = DB::select('SELECT COUNT(*) AS "count",SUM(`amount`) AS "total" FROM `fees` WHERE DATE(`created_at`) BETWEEN "'.$date_from.'" AND "'.$date_to.'"');
            $expense = DB::select('SELECT COUNT(*) AS "count",SUM(`amount`) AS "total" FROM `expense` WHERE DATE(`date`) BETWEEN "'.$date_from.'" AND "'.$date_to.'"');

            // return $list;
            $data['record'] = $record;
            $data['expense'] = $expense;
            $data['date_from'] = $date_from;
            $data['date_to'] = $date_to;
            
        } else {
            $data['record'] = [];
            $data['expense'] = [];
            $data['date_from'] = '';
            $data['date_to'] = '';
        }
        // return $data;
        return view('admin.fees.monthly_collection',$data);
    }

    public function addFees() {
        return view('admin.fees.add');
    }

    public function store(Request $request) {
        try{
            $format_date = Carbon::createFromFormat('d/m/Y', $request->date)->isoFormat('YYYY-MM-DD');
            $validate = $request->validate(Fees::$createRules);
            
            $month = Carbon::parse($format_date)->isoFormat('M');
            $year = Carbon::parse($format_date)->isoFormat('YYYY');

            // return $date = $request->date;
            $check = Fees::where('member_id',$request->member)->whereRaw('MONTH(`date`) = '.$month)->whereRaw('YEAR(`date`) = '.$year)->first();
            if( !$check ){
                $fees = Fees::create([
                    'member_id' => $request->member,
                    'date' => $format_date,
                    'amount' => $request->amount,
                    'package_id' => $request->package_id
                ]);
                Helper::toast("success","Fees has been added.","Success");
                // return back();
                $data['fees'] = $fees;
                return view('admin.fees.fee_slip',$data);
            } else {
                Helper::toast("error","This member fees for this month already paid.","Error");
                return back();
            }
        } catch (\Exception $e){
            Helper::toast("error","Something went wrong.","Error");
            return back();
        }

    }


    public function dueDateReport(Request $request) {
        if( $request->date_from ) {
            // $date_from = Carbon::parse($request->date_from)->isoFormat('YYYY-MM-DD');
            $date_from = Carbon::createFromFormat('d/m/Y', $request->date_from)->isoFormat('YYYY-MM-DD');
            $date_from_formatted = $request->date_from;
            // $date_from = "2021-09-10";
            
            $members = Member::where('status',1)->whereRaw('DAY(join_date) = DAY("'.$date_from.'")')->get();

            $records = [];
            foreach($members as $member) {
                $check_fee = Fees::where('member_id',$member->id)
                ->whereRaw('MONTH(`date`) = MONTH("'.$date_from.'")' )->whereRaw('YEAR(`date`) = YEAR("'.$date_from.'")' )->first();
                if( !$check_fee ) {
                    $records[] = $member;
                }
            }

            // return $records;

            $data['records'] = $records;
            $data['members'] = $members;
            $data['date_from'] = $date_from;
            $data['date_from_formatted'] = $date_from_formatted;
            $data['due_date'] = Carbon::parse($date_from)->isoFormat('DD MMM YYYY');
            
        } else {
            $data['records'] = [];
            $data['members'] = [];
            $data['date_from'] = '';
            $data['date_from_formatted'] = '';
            $data['due_date'] = '';
        }
        return view('admin.fees.due_date_sheet',$data);
    }


}
