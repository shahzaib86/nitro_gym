<?php

namespace App\Http\Controllers\boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Common\Helper;
use Illuminate\Support\Str;
use App\Boiler\Page;

class PageController extends Controller
{
    //
    public function view()
    {
        $data['Pages']= Page::all();
        // return($data);
         return view('admin.boiler.pages.view',$data);

    }

    public function add(Request $request)
    {
        if($request->isMethod('post')){
            $request->validate([
                'title'=>'required',
                'keyword'=>'nullable',
                'description'=>'required',
                'body'=>'required',
            ]);

            $data =[
                'title' => $request->title,
                'slug'=> str::slug($request->title,'-'),
                'keyword' => $request->keyword,
                'description' => $request->description,
                'body' => $request->body,
            ];

            Page::create($data);
            Helper::toast('success','Added');
             
            return redirect(route('pages.show'));

        }else{
            return view('admin.boiler.pages.add');
        }
    }

    public function edit(Request $request,$id){

        if($request->isMethod('post')){

            $request->validate([
                'title'=>'required',
                'keyword'=>'nullable',
                'description'=>'required',
                'body'=>'required',
                ]);

                $data =[
                    'title' => $request->title,
                    'slug'=> str::slug($request->title,'-'),
                    'keyword' => $request->keyword,
                    'description' => $request->description,
                    'body' => $request->body,
                ];

                Page::find($id)->update($data);
                    Helper::toast('success','Updated');
             
                    return redirect(route('pages.show'));
        
        }
            else{

                $pages_result = Page::find($id);
                return view('admin.boiler.pages.edit',compact('pages_result'));

            }
    }
     
    // public function delete($id){
    //     $query = Pages::where('id',$id)->delete();
    //     Helper::toast('success','Deleted');
    //         return back();
    // }
}
