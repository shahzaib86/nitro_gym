<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Boiler\EmailTemplate;

class EmailTemplateController extends Controller
{
    //
    public function view(){


        $data['EmailTemplates']= EmailTemplate::all();
        // return($data);
         return view('admin.boiler.emailtemplate.view',$data);


    }

    public function edit(Request $request,$id){

        if($request->isMethod('post')){

                $request->validate([
                    'template'=>'required',
                    'type'=>'required',
                    ]);

                    $data =[
                        'template' => $request->template,
                        'type' => $request->type,
                    ];
                    EmailTemplate::find($id)->update($data);
                return redirect(route('email.template.show')); 
        }
            else{

                $emailtemplate = EmailTemplate::find($id);
                return view('admin.boiler.emailtemplate.edit',compact('emailtemplate'));

            }
    }
}
