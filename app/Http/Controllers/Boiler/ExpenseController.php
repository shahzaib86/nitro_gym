<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Common\Helper;
use Auth;
use DB;
use Yajra\Datatables\Datatables;
use App\Expense;

class ExpenseController extends Controller
{
    //
    public function index(Request $request){
        return view('admin.expense.index');
    }

    public function fetch(Request $request){

        $query = Expense::where('id','<>',0);

        if( $request->date_from != '' && $request->date_to != '' ){
            $query = $query->whereRaw('DATE(date) BETWEEN "'.$request->date_from.'" AND "'.$request->date_to.'"');
        }

        $query = $query->with(['user']);

        return Datatables::of($query)->addIndexColumn()->make(true);

    }

    public function create() {
        $data['item'] = null;
        return view('admin.expense.add',$data);
    }

    public function store(Request $request) {
        try{
            $validate = $request->validate(Expense::$createRules);

            $fees = Expense::create([
                'user_id' => Auth::user()->id,
                'date' => $request->date,
                'amount' => $request->amount,
                'note' => $request->note
            ]);
            Helper::toast("success","Expense has been added.","Success");
            return back();
            
            
        } catch (\Exception $e){
            Helper::toast("error","Something went wrong.","Error");
            return back();
        }

    }

    public function edit($id) {
        $data['item'] = Expense::findOrFail($id);
        return view('admin.expense.add',$data);
    }

    public function update(Request $request,$id) {
        try{
            $validate = $request->validate(Expense::$createRules);

            $fees = Expense::where('id',$id)->update([
                'date' => $request->date,
                'amount' => $request->amount,
                'note' => $request->note
            ]);
            Helper::toast("success","Expense has been update.","Success");
            return back();
            
        } catch (\Exception $e){
            Helper::toast("error","Something went wrong.","Error");
            return back();
        }

    }

    public function delete($id){
        Expense::find($id)->delete();
        Helper::toast('success','Deleted');
        return back();
    }


}
