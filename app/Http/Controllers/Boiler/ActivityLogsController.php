<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Boiler\Activity as Activity;
use App\Boiler\ActivityTemplate;
use App\Http\Common\Helper;
use Auth;
use Illuminate\Support\Str;
use Yajra\Datatables\Datatables;


class ActivityLogsController extends Controller
{
    //


    public function view ()
    {
     return view('admin.boiler.activitylogs.view');

    }

    public function fetch(Request $request)
    {
        $data=$request->users;
        $query = Activity::get_admin_logs($data);
         return Datatables::of($query)->addIndexColumn()->make(true);

    }
}
