<?php

namespace App\Http\Controllers\Boiler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Boiler\BlogCategory;
use Illuminate\Support\Str;
use App\Http\Common\Helper;

class BlogCategoryController extends Controller
{

    public function view() {
        $data['blogscategories']= BlogCategory::all();
        return view('admin.boiler.blogs.blogs_category',$data);
    }

    public function add(Request $request) {
        if($request->isMethod('post')){
            $request->validate([
                'title'=>'required',
                'description'=>'required',
            ]);

            $data =[
                'title' => $request->title,
                'slug'=> str::slug($request->title,'-'),
                'description' => $request->description,
            ];

            if($request->status){
                $data['status'] = 1;
            } else{
                $data['status'] = 0;
            }

            if($request->featured){
                $data['featured'] = 1;
            } else {
                $data['featured'] = 0;
            }

            BlogCategory::create($data);
            Helper::toast('success','Added');
             
            return redirect(route('blogs_category.show'));

        }else{
            return view('admin.boiler.blogs.blogs_category_add');
        }
    }


    public function edit(Request $request,$id) {

        if($request->isMethod('post')){
            $request->validate([
                'title'=>'required',
                'description'=>'required',
            ]);

            $data =[
                'title' => $request->title,
                'slug'=> str::slug($request->title,'-'),
                'description' => $request->description,
            ];

            if($request->status){
                $data['status'] = 1;
            } else {
                $data['status'] = 0;
            }

            if($request->featured){
                $data['featured'] = 1;
            } else {
                $data['featured'] = 0;
            }

            BlogCategory::find($id)->update($data);
            Helper::toast('success','Updated');
             
            return redirect(route('blogs_category.show'));
        
        } else {
            $categories_result = BlogCategory::find($id);
            return view('admin.boiler.blogs.blogs_category_edit',compact('categories_result'));
        }
    }
        
    public function delete($id) {
        BlogCategory::where('id',$id)->delete();
        Helper::toast('success','Deleted');
        return back();
    }

}
