<?php

namespace App\Boiler;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    //    
    protected $table = 'b_email_template';
    protected $fillable =['template','type'];
}
