<?php

namespace App\Boiler;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    //
    protected $table = 'b_blog_categories';
    protected $fillable=[
        'title','slug','description','featured','status'
    ];
    protected $guarded=[];
    public function blog()
    {
        return $this->hasMany(Blog::class);
    }
}
