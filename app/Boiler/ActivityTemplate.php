<?php

namespace App\Boiler;

use Illuminate\Database\Eloquent\Model;

class ActivityTemplate extends Model
{
    //
    protected $table = 'b_activity_template';
}
