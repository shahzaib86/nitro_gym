<?php

namespace App\Boiler;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = "b_permissions";
    public $timestamps = false;

    public function roles() {
        return $this->belongsToMany('App\Boiler\Role','b_roles_permissions');
    }
     
}
