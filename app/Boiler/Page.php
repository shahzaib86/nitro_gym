<?php

namespace App\Boiler;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //
    protected $table = 'b_page';
    protected $fillable=[ 'id' ,'title','slug','description','keyword','body' ];
    public $timestamps = false;
}
