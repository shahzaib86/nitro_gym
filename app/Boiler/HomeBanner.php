<?php

namespace App\Boiler;

use Illuminate\Database\Eloquent\Model;

class HomeBanner extends Model
{
    protected $table = 'b_home_banner';
    protected $fillable = ['title','banner_path','link','is_outside','status','sequence'];
    public $timestamps = false;
    
    public function getBannerPathAttribute($value) {
        return asset('storage/'.$value);
    }
}
