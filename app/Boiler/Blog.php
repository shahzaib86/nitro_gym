<?php

namespace App\Boiler;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //
    protected $table = 'b_blogs';
    protected $fillable=[
      'blog_category_id','auther_id','title','image','slug','description','featured','status'
    ];

    public function category() {
        return $this->belongsTo('App\Boiler\BlogCategory','blog_category_id');
    }
}
