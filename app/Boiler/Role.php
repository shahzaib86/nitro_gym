<?php

namespace App\Boiler;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'b_roles';
    protected $fillable = ['name'];
    public $timestamps = false;

    public function permissions() {
        return $this->belongsToMany('App\Boiler\Permission','b_roles_permissions');
    }
    
    public function users() {
        return $this->belongsToMany('App\User','b_users_roles');
    }
}
