<?php

namespace App\Boiler;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'b_settings';
    protected $fillable=['value'];
    public $timestamps = false;

    // To get multiple configuration values
    static public function getValues($params) {
        $rows = setting::whereIn('key',$params)->get(['key','value']);
        $data = [];
        foreach($rows as $r) {
            $data[$r['key']] = $r['value'];
        }
        return $data;
    }

    // To get single configuration value
    static public function getValue($param) {
        $row = setting::where('key',$param)->first(['key','value']);
        return $row->value;
    }

    public function getJsonParamsAttribute($value) {
        return json_decode($value,TRUE);
    }
}
