<?php

namespace App\Boiler;


use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Activity extends Model
{
    //
    protected $table = 'b_activity_log';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function template(){
    	return $this->belongsTo('App\Boiler\ActivityTemplate','activity_template_id');
    }

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }
    public static function get_user_logs($user_id,$limit = 5) {
        $result = Activity::where('user_id',$user_id)->with('template')->orderBy('id','desc')->limit($limit)->get();
        $arrange = [];
        foreach($result as $k=>$v ) {
            $arrange[] = [
                'text' => str_ireplace('({user})', 'You', $v['template']['template']),
                'time' => Carbon::parse($v['activity_time'])->isoFormat('DD MMM YYYY Oh:mm a (ddd)')
            ];
        }
        return $arrange;
    }

    public static function get_admin_logs($users=null) {
        $result = Activity::where('user_id','!=',0);
        if( $users != ''  )
            {

                $result = $result->whereIn('user_id',$users);
            }
        $result = $result->with('template','user')->orderBy('id','desc');

        $result = $result->get();

        $arrange = [];
        foreach($result as $k=>$v ) {

            $text = str_ireplace('({user})', $v['user']['name'], $v['template']['template']);
            $params = json_decode($v['json_params'],TRUE);
            foreach( array_keys( $params ) as $keys ) {
                $text = str_ireplace('({'.$keys.'})', $params[$keys], $text);
            }

            $result[$k] =[
                'user' => $v['user']['name'],
                'text' => $text,
                'time' => Carbon::parse($v['activity_time'])->isoFormat('DD MMM YYYY Oh:mm a (ddd)'),
                'params' => $v['json_params']
            ];

        }
         return $result;

    }

}
